<?php

namespace App\Http\Controllers\Admin\Group;

use App\Http\Controllers\Controller;
use App\Models\Group;
use Illuminate\Http\Request;

class GroupController extends Controller
{
    public function index(){
        $data = Group::all();
        return view('admin.group.index',compact('data'));
    }

    public function create(){
        return view('admin.group.groupForm');
    }

    public function store(Request $request){
        $request->validate([
            "name"=>"required",
            "code"=>"required|unique:groups,code",
            "status"=>"required"
        ]);
       $group = new Group();
       $group->name = $request->name;
       $group->code = $request->code;
       $group->status = $request->status;
       $group->save();

       return redirect()->route('admin.group')->with('success',"Group Created Successfully!");
    }

    public function edit($id){
        $group = Group::find($id);
        return view('admin.group.groupForm',compact('group'));
    }

    public function show($id){
        $group = Group::find($id);
        $isView= true;
        return view('admin.group.groupForm',compact('group','isView'));
    }

    public function update(Request $request,$id){
        $group = Group::find($id);
        $group->name = $request->name;
        $group->code = $request->code;
        $group->status = $request->status;
        $group->save();
        return redirect()->route('admin.group')->with('success',"Group Update Successfully!");
    }

    public function delete($id){
        $group = Group::find($id);
        $group->delete();
        return redirect()->route('admin.group')->with('success',"Group Delete Successfully!");
    }
}
