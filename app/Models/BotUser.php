<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class BotUser extends Model
{
    use HasFactory,Notifiable;

    protected $table="bot_users";

    protected $fillable=['first_name','is_email_verified', 'last_name', 'username', 'referral', 'chat_id', 'type', 'join_on', 'email', 'phone', 'referred_by', 'bot_type'];
}
