<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MessagesWithBot extends Model
{
    use HasFactory;
    protected $table="messages_with_bots";

    protected $fillable=['message', 'message_id', 'message_on', 'message_from', 'message_to', 'chat_id'];
}
