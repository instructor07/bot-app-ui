<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProcessChat extends Model
{
    use HasFactory;
    protected $fillable = ['chat_id', 'event_name', 'row_id', 'value', 'is_done'];
}
