<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class QuestionQuize extends Model
{
    use HasFactory;
    protected $table="quiz_questions";
    protected $fillable=['question_id', 'quiz_id', 'notes', 'expire_time'];

    public function quiz(){
        return $this->belongsTo(Quiz::class,'quiz_id');
    }

    public function question(){
        return $this->belongsTo(Question::class,'question_id');
    }

    
}
