<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Quiz extends Model
{
    use HasFactory;
    protected $table="quizzes";
    protected $fillable=['name', 'description', 'rules', 'status'];

    public function questionQuiz()
    {
        return $this->hasMany(QuestionQuize::class,'quiz_id');
    }

    public function quizCode()
    {
        return $this->hasOne(QuizCode::class);
    }
}
