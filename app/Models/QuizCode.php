<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class QuizCode extends Model
{
    use HasFactory;
    protected $table = "quiz_codes";
    protected $fillable = ['quiz_id','code','telegram_id'];

    public function quiz(){
        return $this->belongsTo(Quiz::class);
    }
}
