-- phpMyAdmin SQL Dump
-- version 4.9.7deb1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Feb 03, 2023 at 07:46 AM
-- Server version: 8.0.25
-- PHP Version: 8.0.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `telegram_bot_elarning`
--

-- --------------------------------------------------------

--
-- Table structure for table `answered_users`
--

CREATE TABLE `answered_users` (
  `id` bigint UNSIGNED NOT NULL,
  `quiz_start_details_question_id` bigint UNSIGNED DEFAULT NULL,
  `bot_user_id` bigint UNSIGNED DEFAULT NULL,
  `poll_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_telegram_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `answer` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `bot_users`
--

CREATE TABLE `bot_users` (
  `id` bigint UNSIGNED NOT NULL,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `referral` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `chat_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `join_on` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `referred_by` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bot_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `bot_users`
--

INSERT INTO `bot_users` (`id`, `first_name`, `last_name`, `username`, `referral`, `chat_id`, `type`, `join_on`, `email`, `phone`, `referred_by`, `bot_type`, `created_at`, `updated_at`) VALUES
(1, 'Nishant', 'Kumar', 'nishantkumar656', '2067178683', '2067178683', 'private', '2023-01-22 14:53:20', NULL, NULL, NULL, 'telegram', '2023-01-22 09:23:20', '2023-01-22 09:23:20'),
(2, 'DemoInclick24bot', NULL, 'DemoInclick24bot', '1881661551', '1881661551', 'private', '2023-01-31 19:26:05', NULL, NULL, NULL, 'telegram', '2023-01-31 13:56:05', '2023-01-31 13:56:05');

-- --------------------------------------------------------

--
-- Table structure for table `configurations`
--

CREATE TABLE `configurations` (
  `id` bigint UNSIGNED NOT NULL,
  `value` json DEFAULT NULL,
  `default` json NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--

CREATE TABLE `contacts` (
  `id` bigint UNSIGNED NOT NULL,
  `number` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subject` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `errors`
--

CREATE TABLE `errors` (
  `id` bigint UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `events`
--

CREATE TABLE `events` (
  `id` bigint UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `location` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date` date NOT NULL,
  `time` time NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `events`
--

INSERT INTO `events` (`id`, `title`, `description`, `image`, `location`, `date`, `time`, `created_at`, `updated_at`) VALUES
(1, 'Sed et accusamus sed nulla fuga tempora occaecati.', 'Ipsa aliquam itaque quos voluptatem fuga voluptates. Placeat id qui explicabo consequatur. Quibusdam necessitatibus saepe qui omnis doloribus dolor similique eveniet. Vel molestiae et et est.', 'https://via.placeholder.com/300x300.png/006611?text=omnis', '33914 Grace Crossroad Suite 688\nWest Danykaview, IN 02978', '2023-01-05', '15:58:53', '2022-07-23 06:40:06', '2022-07-23 06:40:06'),
(2, 'Enim omnis voluptatum nam.', 'Iusto sapiente quos quos sunt voluptates. Nihil qui aut quis neque rerum excepturi. Ut dolor voluptas ipsam velit qui.', 'https://via.placeholder.com/300x300.png/006622?text=vitae', '346 Beer Terrace Apt. 521\nPiperview, OH 48085-4094', '2022-08-03', '17:11:39', '2022-07-23 06:40:06', '2022-07-23 06:40:06'),
(3, 'Aut est et nihil temporibus molestiae nesciunt eos.', 'Nihil quia iste nemo. Veritatis labore qui itaque quasi. Excepturi laborum unde explicabo asperiores reprehenderit praesentium. Quos atque iste delectus rerum quis aut.', 'https://via.placeholder.com/300x300.png/00aa44?text=distinctio', '847 Osinski View\nEast Magdalenbury, NE 94026-3077', '2022-10-26', '18:04:18', '2022-07-23 06:40:06', '2022-07-23 06:40:06'),
(4, 'Labore cupiditate eaque iusto quasi voluptates labore fugit.', 'Consequuntur sapiente provident placeat sit. Sit aut non voluptate est quam iusto est. At aut occaecati maiores ut qui architecto consectetur quis.', 'https://via.placeholder.com/300x300.png/00ffff?text=expedita', '370 Nikita Cliff\nWest Grayson, LA 66555-9461', '2023-06-07', '15:47:43', '2022-07-23 06:40:06', '2022-07-23 06:40:06'),
(5, 'Vel molestias eos cum laborum.', 'Maiores et incidunt repellendus voluptatem sunt at non. Non laborum dicta ipsum eum voluptatem repellat. Optio voluptatem sed voluptatem ut iste mollitia est dolores.', 'https://via.placeholder.com/300x300.png/009988?text=tempora', '68748 Quinton Field\nFraneckibury, MD 58456-9584', '2022-08-19', '19:34:30', '2022-07-23 06:40:06', '2022-07-23 06:40:06'),
(6, 'Earum aut quidem tempora deserunt repellendus recusandae.', 'Aliquid enim id omnis voluptas quos animi. Repellendus tempore culpa porro nihil eum tempore odit. Molestias sunt ut minima.', 'https://via.placeholder.com/300x300.png/0000dd?text=ut', '97732 Greenholt Cove Apt. 012\nDoylemouth, NJ 25500-9912', '2023-06-25', '21:23:27', '2022-07-23 06:40:06', '2022-07-23 06:40:06'),
(7, 'Totam nemo eos vero rerum qui perferendis ipsum.', 'Minima porro neque sunt magni consequatur. Tempora voluptas reiciendis rerum quidem porro eos. Dolores a et asperiores quia unde vel natus. Ea voluptatem ut aut natus veniam nemo id. Iure mollitia quis excepturi est.', 'https://via.placeholder.com/300x300.png/009900?text=asperiores', '6166 Fritsch Dam\nOzellaview, AZ 67626', '2023-01-14', '12:15:15', '2022-07-23 06:40:06', '2022-07-23 06:40:06'),
(8, 'Dolorum consequuntur consectetur ipsum laudantium.', 'Debitis inventore neque sit. Occaecati quidem et labore qui non fuga sed. Molestias cum eveniet et in libero error.', 'https://via.placeholder.com/300x300.png/00aaaa?text=maiores', '4906 Tavares Terrace\nSwiftborough, CT 99076', '2023-05-31', '18:12:26', '2022-07-23 06:40:06', '2022-07-23 06:40:06'),
(9, 'Quo enim hic nisi vero magni architecto ut ea.', 'Eius earum non dolor laborum. Incidunt voluptatem fugit non consequuntur. Inventore dolores qui ut vero vitae omnis molestiae.', 'https://via.placeholder.com/300x300.png/00bbcc?text=placeat', '40009 Braden Lodge Suite 771\nKshlerinton, SC 54433', '2022-08-16', '03:41:26', '2022-07-23 06:40:06', '2022-07-23 06:40:06'),
(10, 'Ullam est voluptas blanditiis voluptatum eum molestias eius.', 'Quis temporibus rerum ea rerum. Dolorem eius rerum a perferendis quisquam minus nostrum unde. Corporis occaecati omnis aliquam eaque voluptatem et. Non maiores cumque incidunt qui.', 'https://via.placeholder.com/300x300.png/00aabb?text=fugiat', '23198 Wisoky Locks\nNew Santiagoberg, IA 76756', '2022-10-03', '19:30:19', '2022-07-23 06:40:06', '2022-07-23 06:40:06'),
(11, 'Beatae ut quo veniam reprehenderit ipsam minus tenetur dolorem.', 'Distinctio sint accusantium molestiae beatae sapiente. Itaque dolores impedit quis perferendis sit alias. Ipsa quia corrupti officia.', 'https://via.placeholder.com/300x300.png/0022ff?text=quia', '3351 Heidi Circles\nGunnarchester, WI 38244', '2022-07-27', '12:28:35', '2022-07-23 09:32:41', '2022-07-23 09:32:41'),
(12, 'Ullam occaecati et necessitatibus non quia et.', 'Quidem vero dolor et provident rem officia repudiandae. Modi iusto voluptas optio ex. Consequatur delectus incidunt reiciendis accusamus eum.', 'https://via.placeholder.com/300x300.png/008899?text=recusandae', '363 Anderson Views Apt. 837\nEast Fletcher, ME 51361-1899', '2023-02-23', '02:48:53', '2022-07-23 09:32:41', '2022-07-23 09:32:41'),
(13, 'Et illum quo ut.', 'Vel impedit rerum soluta explicabo sit. Saepe dignissimos et fugiat quis sapiente perferendis. Sit id est vel.', 'https://via.placeholder.com/300x300.png/00ff44?text=veritatis', '180 Prohaska Island Suite 842\nQuigleychester, NE 45124', '2023-01-28', '21:18:27', '2022-07-23 09:32:41', '2022-07-23 09:32:41'),
(14, 'Voluptatem reprehenderit soluta provident aut aliquid delectus.', 'Et magnam suscipit perferendis sit dolorem repellendus. Labore natus asperiores debitis est repudiandae iusto. Cum non dolor non ut quas maxime laboriosam. Beatae omnis aliquam voluptatem.', 'https://via.placeholder.com/300x300.png/00ff44?text=nisi', '9865 Candace Shoal\nGibsonshire, FL 32915', '2022-08-09', '18:52:42', '2022-07-23 09:32:41', '2022-07-23 09:32:41'),
(15, 'Magni modi fuga facere aut fuga sed.', 'Praesentium voluptates quos temporibus amet. Et voluptate quo quaerat eaque ut ad molestiae quasi. Vel tempore esse corrupti harum eius perferendis adipisci.', 'https://via.placeholder.com/300x300.png/00cc11?text=voluptate', '39874 Renee Radial Apt. 779\nMontestad, HI 85123', '2023-02-04', '05:16:05', '2022-07-23 09:32:41', '2022-07-23 09:32:41'),
(16, 'Quas commodi est porro qui et.', 'Optio a qui qui voluptatem eos. Consequuntur earum dicta aliquid repudiandae laudantium. Itaque provident ut incidunt amet tenetur asperiores laudantium sint.', 'https://via.placeholder.com/300x300.png/0022aa?text=dolorem', '774 Ervin Isle Apt. 128\nMetzburgh, MI 61990', '2023-01-02', '00:39:56', '2022-07-23 09:32:41', '2022-07-23 09:32:41'),
(17, 'Est accusantium occaecati et sed aut enim.', 'Dolor aut quisquam quis cumque delectus non reprehenderit. Atque a et odio est et ipsa. Optio ut placeat temporibus qui.', 'https://via.placeholder.com/300x300.png/0000dd?text=quaerat', '6781 Wyman Tunnel\nHellerville, KS 94584', '2022-07-28', '05:14:38', '2022-07-23 09:32:41', '2022-07-23 09:32:41'),
(18, 'Non autem laboriosam non excepturi dolorem.', 'Nisi non ipsum non ut enim cumque dignissimos. Quia repudiandae totam eius ea est facere commodi. Veritatis maiores impedit ab quia iste eum.', 'https://via.placeholder.com/300x300.png/00ddbb?text=ad', '4323 Cassin Walks\nEast Maryjanefort, WA 73643-9477', '2022-09-28', '18:58:34', '2022-07-23 09:32:41', '2022-07-23 09:32:41'),
(19, 'Suscipit perspiciatis quidem provident eos et aliquam ut aut.', 'Quasi ea iste voluptate saepe voluptatem esse. Provident quaerat ab esse aspernatur. Optio harum dolor nam debitis.', 'https://via.placeholder.com/300x300.png/002299?text=iusto', '602 Roman Tunnel Suite 963\nPort Graham, WA 91793-9083', '2023-07-15', '10:14:07', '2022-07-23 09:32:41', '2022-07-23 09:32:41'),
(20, 'Ipsum laborum eum doloremque rem.', 'Maiores sint consequatur aperiam beatae voluptas dignissimos id. Dignissimos doloribus culpa iusto voluptatibus temporibus quidem aperiam. Et voluptas quidem in minima. Voluptas quae quis non magnam est aut.', 'https://via.placeholder.com/300x300.png/006611?text=esse', '88113 Michael Grove\nWilhelmineside, IA 66876', '2022-08-31', '12:07:18', '2022-07-23 09:32:41', '2022-07-23 09:32:41');

-- --------------------------------------------------------

--
-- Table structure for table `event_application_forms`
--

CREATE TABLE `event_application_forms` (
  `id` bigint UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cell_phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `education` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `event_token` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_allow` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `event_application_forms`
--

INSERT INTO `event_application_forms` (`id`, `name`, `email`, `cell_phone`, `education`, `event_token`, `is_allow`, `created_at`, `updated_at`) VALUES
(1, 'Nishant Kumar', 'nishantraj656@gmail.com', '786868686868', 'project_manager', NULL, NULL, '2022-07-24 03:24:21', '2022-07-24 03:24:21');

-- --------------------------------------------------------

--
-- Table structure for table `event_topics`
--

CREATE TABLE `event_topics` (
  `id` bigint UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `event_id` bigint UNSIGNED NOT NULL,
  `lecture_date` timestamp NOT NULL,
  `parent` int DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `event_topics`
--

INSERT INTO `event_topics` (`id`, `title`, `description`, `event_id`, `lecture_date`, `parent`, `created_at`, `updated_at`) VALUES
(1, 'Et veniam autem iusto hic.', 'Asperiores tempore expedita neque et porro. Veritatis culpa qui modi sed itaque minima veniam illum. Atque ut laborum accusamus odit ut.', 6, '2023-05-06 19:35:00', NULL, '2022-07-23 06:40:06', '2022-07-24 02:45:26'),
(2, 'In voluptatem tempora dolor est similique.', 'Dolorem magnam maxime expedita. Nulla voluptates id fugit nam magni ut. Sunt asperiores dolorum temporibus qui nesciunt dolor.', 10, '2022-10-15 11:11:26', NULL, '2022-07-23 06:40:06', '2022-07-23 06:40:06'),
(3, 'Magnam et ducimus ducimus enim.', 'Quibusdam qui voluptatem rerum. Debitis et beatae totam labore qui quis aperiam. Odio asperiores numquam eum dolores.', 10, '2023-06-13 11:29:36', NULL, '2022-07-23 06:40:06', '2022-07-23 06:40:06'),
(4, 'Nostrum suscipit sint qui sint ipsam.', 'Rerum modi quia ratione molestiae debitis deserunt. Modi atque dicta quae aut. Deserunt labore distinctio numquam dolorum necessitatibus tenetur.', 2, '2022-12-21 14:56:16', NULL, '2022-07-23 06:40:06', '2022-07-23 06:40:06'),
(5, 'Non et libero distinctio unde at qui alias.', 'Non esse fuga sed cum ipsa quae et et. Similique eum dignissimos ut eligendi illo voluptatum et. Et facilis placeat molestiae sint at. Corrupti corporis libero qui laboriosam dolorem ipsum quibusdam. Molestias ad assumenda ipsam soluta sunt quas quia.', 6, '2023-05-22 02:06:06', NULL, '2022-07-23 06:40:06', '2022-07-23 06:40:06'),
(6, 'Dolores minima eaque corporis iure neque quia.', 'Quisquam sit enim ut impedit. Ipsa animi quas neque qui aliquid consectetur ut. Eaque voluptatem laudantium nostrum. Eveniet fugit est nobis adipisci ipsum.', 2, '2022-11-13 08:24:04', NULL, '2022-07-23 06:40:06', '2022-07-23 06:40:06'),
(7, 'Ad sunt doloremque sapiente.', 'Odit labore animi vitae voluptate ratione cumque sit. Quis quae magni aut sed est enim. Rem esse quia impedit et aut aut.', 3, '2022-09-15 16:55:12', NULL, '2022-07-23 06:40:06', '2022-07-23 06:40:06'),
(8, 'Eius ipsam itaque tenetur perspiciatis et minus qui.', 'Velit illum non suscipit sequi. Nostrum et amet earum sint ex maiores sed molestias. Accusantium beatae harum ut et labore molestias sint.', 9, '2022-11-02 03:50:58', NULL, '2022-07-23 06:40:06', '2022-07-23 06:40:06'),
(9, 'Fugiat ipsum odit repellendus beatae.', 'Asperiores illum hic optio voluptatem. Ut autem mollitia molestias quo.', 4, '2023-04-16 20:34:12', NULL, '2022-07-23 06:40:06', '2022-07-23 06:40:06'),
(10, 'Iure impedit et inventore quibusdam.', 'Et doloribus numquam perspiciatis. Deserunt quaerat praesentium quia exercitationem. Maxime sequi quia nobis aut recusandae vel.', 6, '2022-08-21 13:13:46', NULL, '2022-07-23 06:40:06', '2022-07-23 06:40:06'),
(11, 'Aliquam sed sed et nostrum aliquam.', 'Sint nostrum incidunt blanditiis deleniti fuga labore. Laboriosam odio ratione ea consequatur voluptas quo. Cum rem nihil officia laudantium. Fuga quibusdam optio corporis et enim ut laborum.', 9, '2022-10-28 11:23:36', NULL, '2022-07-23 09:32:41', '2022-07-23 09:32:41'),
(12, 'Suscipit quae sit aperiam distinctio eos.', 'Voluptas error a cupiditate impedit consequatur. Non reprehenderit quasi in qui. In animi est perspiciatis aut ipsum.', 9, '2022-08-29 05:51:14', NULL, '2022-07-23 09:32:41', '2022-07-23 09:32:41'),
(13, 'Dignissimos rerum alias eum porro consequuntur modi.', 'Enim laborum suscipit culpa suscipit. Natus suscipit magnam saepe alias perspiciatis accusantium. Et et qui deserunt quo. Aliquid veniam sit id tenetur expedita aut.', 12, '2022-09-05 20:39:00', NULL, '2022-07-23 09:32:41', '2022-07-24 19:56:15'),
(14, 'Aut iusto aliquam quibusdam sit aut.', 'Doloremque harum ab pariatur enim et rerum. Aut vel aut et et incidunt maxime. Sit quos corrupti earum. Et ipsum et ducimus nihil.', 18, '2022-12-19 02:30:15', NULL, '2022-07-23 09:32:41', '2022-07-23 09:32:41'),
(15, 'Nihil sequi occaecati delectus distinctio.', 'Consequuntur eaque autem necessitatibus itaque. Voluptatem tenetur sed rerum quaerat provident. Unde in quisquam non alias voluptas pariatur odit voluptatibus. Occaecati dolorem harum modi nihil magnam.', 2, '2022-08-01 19:13:48', NULL, '2022-07-23 09:32:41', '2022-07-23 09:32:41'),
(16, 'Consequatur laboriosam consectetur et enim placeat est delectus.', 'Enim culpa et consequuntur ratione. Unde et omnis quam beatae earum iure. Unde et repudiandae consectetur qui non itaque et. Fugit aut et voluptas et sunt.', 16, '2023-07-22 06:13:32', NULL, '2022-07-23 09:32:41', '2022-07-23 09:32:41'),
(17, 'Nulla autem magnam quaerat deserunt.', 'Laborum ea voluptas eligendi. A asperiores quasi repudiandae officia.', 4, '2023-03-28 21:36:23', NULL, '2022-07-23 09:32:41', '2022-07-23 09:32:41'),
(18, 'Quis et dignissimos ut sed est quibusdam dolorem et.', 'Ducimus quisquam suscipit eligendi reprehenderit. Natus fugiat illo et dolores recusandae dicta aliquam magnam. Quasi dolorem beatae architecto qui dignissimos. Nostrum consectetur voluptatem et et.', 6, '2022-10-29 17:14:58', NULL, '2022-07-23 09:32:41', '2022-07-23 09:32:41'),
(19, 'Minus nulla consequuntur qui quisquam est qui.', 'Nobis molestias sint autem. Exercitationem vel dolor omnis reprehenderit inventore porro sed. Debitis nihil et quidem maiores nisi voluptatem quo dolores.', 19, '2023-07-16 01:06:40', NULL, '2022-07-23 09:32:41', '2022-07-23 09:32:41'),
(20, 'Earum soluta nesciunt sunt aut autem id.', 'Esse dignissimos quia dolorem consectetur odit. Et aut quae consectetur quia dolor delectus aut. Neque nobis quas aut dolores quam voluptates alias provident. Eius facilis modi modi praesentium.', 16, '2023-02-15 02:06:26', NULL, '2022-07-23 09:32:41', '2022-07-23 09:32:41');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `jobs`
--

CREATE TABLE `jobs` (
  `id` bigint UNSIGNED NOT NULL,
  `queue` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `attempts` tinyint UNSIGNED NOT NULL,
  `reserved_at` int UNSIGNED DEFAULT NULL,
  `available_at` int UNSIGNED NOT NULL,
  `created_at` int UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `messages_with_bots`
--

CREATE TABLE `messages_with_bots` (
  `id` bigint UNSIGNED NOT NULL,
  `message` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `message_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `message_on` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `message_from` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `message_to` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `chat_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2019_08_19_000000_create_failed_jobs_table', 1),
(3, '2021_07_01_162806_add_telegram_caht_id_to_users', 1),
(4, '2021_07_18_044917_create_bot_users_table', 1),
(5, '2021_07_18_120555_create_messages_with_bots_table', 1),
(6, '2021_07_24_091702_create_verify_bot_mails_table', 1),
(7, '2021_11_06_075117_add_preferred_time', 1),
(8, '2022_01_16_102240_create_errors_table', 1),
(9, '2022_02_01_172949_add_number_to_contacts_table', 2),
(10, '2022_02_01_173838_make_nullable_message_column_in_contacts_table', 2),
(11, '2022_07_23_112544_create_event_table', 3),
(12, '2022_07_23_112754_create_event_topics_table', 4),
(13, '2022_12_30_061914_create_configrations_table', 5),
(14, '2023_01_02_175148_create_quizes_table_', 5),
(15, '2023_01_02_175221_create_questions_table', 6),
(16, '2023_01_03_030325_create_options_table', 7),
(17, '2023_01_03_031224_create_quizze_questions__table', 7),
(18, '2023_01_20_160103_create_quiz_code', 8),
(19, '2023_01_23_195834_create_process_chats_table', 9),
(20, '2023_01_29_120712_create_quiz_start_details_table', 10),
(21, '2023_01_29_180901_create_quiz_start_details_questions_table', 10),
(22, '2023_01_29_181843_create_answered_users_table', 10),
(23, '2023_01_30_025057_create_jobs_table', 11);

-- --------------------------------------------------------

--
-- Table structure for table `options`
--

CREATE TABLE `options` (
  `id` bigint UNSIGNED NOT NULL,
  `text` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `isAnswer` tinyint(1) NOT NULL DEFAULT '0',
  `question_id` bigint UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `options`
--

INSERT INTO `options` (`id`, `text`, `isAnswer`, `question_id`, `created_at`, `updated_at`) VALUES
(1, 'Shakha', 0, 1, '2023-01-06 14:52:22', '2023-01-06 14:52:22'),
(2, 'Charna', 1, 1, '2023-01-06 14:52:22', '2023-01-06 14:52:22'),
(3, 'Ratha', 0, 1, '2023-01-06 14:52:22', '2023-01-06 14:52:22'),
(4, 'Yajna', 0, 1, '2023-01-06 14:52:22', '2023-01-06 14:52:22'),
(5, 'Commodities became cheap', 0, 2, '2023-01-06 14:52:22', '2023-01-06 14:52:22'),
(6, 'Gold Mining was stalled', 0, 2, '2023-01-06 14:52:22', '2023-01-06 14:52:22'),
(7, 'Money economy was gradually replaced by Barter Economy', 0, 2, '2023-01-06 14:52:22', '2023-01-06 14:52:22'),
(8, 'There was a decline in trade', 1, 2, '2023-01-06 14:52:22', '2023-01-06 14:52:22'),
(9, 'Nagananda', 0, 3, '2023-01-06 14:52:22', '2023-01-06 14:52:22'),
(10, 'Ratnavali', 0, 3, '2023-01-06 14:52:22', '2023-01-06 14:52:22'),
(11, 'Priyadarshika', 0, 3, '2023-01-06 14:52:22', '2023-01-06 14:52:22'),
(12, 'All of these', 1, 3, '2023-01-06 14:52:22', '2023-01-06 14:52:22'),
(13, 'Ravikirti', 1, 4, '2023-01-06 14:52:22', '2023-01-06 14:52:22'),
(14, 'Hiuen Tsang', 0, 4, '2023-01-06 14:52:22', '2023-01-06 14:52:22'),
(15, 'Bharavi', 0, 4, '2023-01-06 14:52:22', '2023-01-06 14:52:22'),
(16, 'Dandin', 0, 4, '2023-01-06 14:52:22', '2023-01-06 14:52:22'),
(17, 'Mahapadmananda', 1, 5, '2023-01-06 14:52:22', '2023-01-06 14:52:22'),
(18, 'Shisunaga', 0, 5, '2023-01-06 14:52:22', '2023-01-06 14:52:22'),
(19, 'Dhanananda', 0, 5, '2023-01-06 14:52:22', '2023-01-06 14:52:22'),
(20, 'Nandivardhan', 0, 5, '2023-01-06 14:52:22', '2023-01-06 14:52:22'),
(21, 'Samudragupta', 1, 6, '2023-01-06 14:52:22', '2023-01-06 14:52:22'),
(22, 'Kumargupta', 0, 6, '2023-01-06 14:52:22', '2023-01-06 14:52:22'),
(23, 'Skandgupta', 0, 6, '2023-01-06 14:52:22', '2023-01-06 14:52:22'),
(24, 'Chandragupta II', 0, 6, '2023-01-06 14:52:22', '2023-01-06 14:52:22'),
(25, 'Junagarh in Gujarat', 0, 7, '2023-01-06 14:52:22', '2023-01-06 14:52:22'),
(26, 'Ranchi in Jharkhand', 0, 7, '2023-01-06 14:52:22', '2023-01-06 14:52:22'),
(27, 'Bhabru in Rajasthan', 0, 7, '2023-01-06 14:52:22', '2023-01-06 14:52:22'),
(28, 'Lumbini in Nepal', 1, 7, '2023-01-06 14:52:22', '2023-01-06 14:52:22'),
(29, 'Vinaya Pitaka', 1, 8, '2023-01-06 14:52:22', '2023-01-06 14:52:22'),
(30, 'Sutta Pitaka', 0, 8, '2023-01-06 14:52:22', '2023-01-06 14:52:22'),
(31, 'Abhidhamma Pitaka', 0, 8, '2023-01-06 14:52:22', '2023-01-06 14:52:22'),
(32, 'None of the above', 0, 8, '2023-01-06 14:52:22', '2023-01-06 14:52:22'),
(33, 'Pandyas', 0, 9, '2023-01-06 14:52:22', '2023-01-06 14:52:22'),
(34, 'Kushans', 0, 9, '2023-01-06 14:52:22', '2023-01-06 14:52:22'),
(35, 'Nandas', 0, 9, '2023-01-06 14:52:22', '2023-01-06 14:52:22'),
(36, 'Sungas', 1, 9, '2023-01-06 14:52:22', '2023-01-06 14:52:22'),
(37, 'Ladakh', 1, 10, '2023-01-06 14:52:22', '2023-01-06 14:52:22'),
(38, 'Orissa', 0, 10, '2023-01-06 14:52:22', '2023-01-06 14:52:22'),
(39, 'Kuchh', 0, 10, '2023-01-06 14:52:22', '2023-01-06 14:52:22'),
(40, 'Assam', 0, 10, '2023-01-06 14:52:22', '2023-01-06 14:52:22'),
(41, 'As an export commodity.', 0, 11, '2023-01-06 14:52:22', '2023-01-06 14:52:22'),
(42, 'As a medium of exchange in place of coins.', 0, 11, '2023-01-06 14:52:22', '2023-01-06 14:52:22'),
(43, 'As amulets and charms to ward off evil spirits.', 0, 11, '2023-01-06 14:52:22', '2023-01-06 14:52:22'),
(44, 'For marking their goods and property.', 1, 11, '2023-01-06 14:52:22', '2023-01-06 14:52:22'),
(45, '1 Only', 0, 12, '2023-01-06 14:52:22', '2023-01-06 14:52:22'),
(46, '2 Only', 0, 12, '2023-01-06 14:52:22', '2023-01-06 14:52:22'),
(47, 'Both 1 & 2', 1, 12, '2023-01-06 14:52:22', '2023-01-06 14:52:22'),
(48, 'Neither 1 nor 2', 0, 12, '2023-01-06 14:52:22', '2023-01-06 14:52:22'),
(49, 'Charaka', 0, 13, '2023-01-06 14:52:22', '2023-01-06 14:52:22'),
(50, 'Sushena', 0, 13, '2023-01-06 14:52:22', '2023-01-06 14:52:22'),
(51, 'Jivaka', 1, 13, '2023-01-06 14:52:22', '2023-01-06 14:52:22'),
(52, 'Vagbhatta', 0, 13, '2023-01-06 14:52:22', '2023-01-06 14:52:22'),
(53, 'Only 1 & 2', 0, 14, '2023-01-06 14:52:22', '2023-01-06 14:52:22'),
(54, 'Only 1 & 3', 1, 14, '2023-01-06 14:52:22', '2023-01-06 14:52:22'),
(55, '1, 2 & 3', 0, 14, '2023-01-06 14:52:22', '2023-01-06 14:52:22'),
(56, 'Only 3', 0, 14, '2023-01-06 14:52:22', '2023-01-06 14:52:22'),
(57, 'Only 1', 1, 15, '2023-01-06 14:52:22', '2023-01-06 14:52:22'),
(58, 'Only 1 & 3', 0, 15, '2023-01-06 14:52:22', '2023-01-06 14:52:22'),
(59, '1, 2 & 3', 0, 15, '2023-01-06 14:52:22', '2023-01-06 14:52:22'),
(60, 'None', 0, 15, '2023-01-06 14:52:22', '2023-01-06 14:52:22'),
(61, 'Only 1, 2 & 3', 1, 16, '2023-01-06 14:52:22', '2023-01-06 14:52:22'),
(62, 'Only 1, 3 & 4', 0, 16, '2023-01-06 14:52:22', '2023-01-06 14:52:22'),
(63, 'Only 2, 3 & 4', 0, 16, '2023-01-06 14:52:22', '2023-01-06 14:52:22'),
(64, '1, 2, 3 & 4', 0, 16, '2023-01-06 14:52:22', '2023-01-06 14:52:22'),
(65, '1 & 2 Only', 0, 17, '2023-01-06 14:52:22', '2023-01-06 14:52:22'),
(66, '3 Only', 0, 17, '2023-01-06 14:52:22', '2023-01-06 14:52:22'),
(67, '2 & 3 Only', 0, 17, '2023-01-06 14:52:22', '2023-01-06 14:52:22'),
(68, '1, 2 & 3', 1, 17, '2023-01-06 14:52:22', '2023-01-06 14:52:22'),
(69, '1 & 2 Only', 0, 18, '2023-01-06 14:52:22', '2023-01-06 14:52:22'),
(70, '3 Only', 0, 18, '2023-01-06 14:52:22', '2023-01-06 14:52:22'),
(71, '2 & 3 Only', 0, 18, '2023-01-06 14:52:22', '2023-01-06 14:52:22'),
(72, '1, 2 & 3', 1, 18, '2023-01-06 14:52:22', '2023-01-06 14:52:22'),
(73, 'Democracy', 0, 19, '2023-01-06 14:52:22', '2023-01-06 14:52:22'),
(74, 'Socialist', 0, 19, '2023-01-06 14:52:22', '2023-01-06 14:52:22'),
(75, 'Autocratic', 0, 19, '2023-01-06 14:52:22', '2023-01-06 14:52:22'),
(76, 'Republic', 1, 19, '2023-01-06 14:52:22', '2023-01-06 14:52:22'),
(77, '1, 2 & 3 Only', 1, 17, '2023-01-06 14:52:22', '2023-01-06 14:52:22'),
(78, '2, 3 & 4 Only', 0, 17, '2023-01-06 14:52:22', '2023-01-06 14:52:22'),
(79, '1, 3 & 4 Only', 0, 17, '2023-01-06 14:52:22', '2023-01-06 14:52:22'),
(80, '1, 2, 3 & 4', 0, 17, '2023-01-06 14:52:22', '2023-01-06 14:52:22'),
(81, 'Anga', 0, 20, '2023-01-06 14:52:23', '2023-01-06 14:52:23'),
(82, 'Magadha', 0, 20, '2023-01-06 14:52:23', '2023-01-06 14:52:23'),
(83, 'Malla', 1, 20, '2023-01-06 14:52:23', '2023-01-06 14:52:23'),
(84, 'Vatsa', 0, 20, '2023-01-06 14:52:23', '2023-01-06 14:52:23'),
(85, 'Dholavira', 0, 21, '2023-01-06 14:52:23', '2023-01-06 14:52:23'),
(86, 'Ellora', 0, 21, '2023-01-06 14:52:23', '2023-01-06 14:52:23'),
(87, 'Bhimbetka', 1, 21, '2023-01-06 14:52:23', '2023-01-06 14:52:23'),
(88, 'Soan Valley', 0, 21, '2023-01-06 14:52:23', '2023-01-06 14:52:23'),
(89, 'Rajan', 0, 22, '2023-01-06 14:52:23', '2023-01-06 14:52:23'),
(90, 'Jana', 0, 22, '2023-01-06 14:52:23', '2023-01-06 14:52:23'),
(91, 'Kulapa', 1, 22, '2023-01-06 14:52:23', '2023-01-06 14:52:23'),
(92, 'Vis', 0, 22, '2023-01-06 14:52:23', '2023-01-06 14:52:23'),
(93, 'Only 1', 0, 23, '2023-01-06 14:52:23', '2023-01-06 14:52:23'),
(94, 'Only 1 & 2', 0, 23, '2023-01-06 14:52:23', '2023-01-06 14:52:23'),
(95, 'Only 2 & 3', 0, 23, '2023-01-06 14:52:23', '2023-01-06 14:52:23'),
(96, '1, 2 & 3', 1, 23, '2023-01-06 14:52:23', '2023-01-06 14:52:23'),
(97, 'Ahimsa', 0, 24, '2023-01-06 14:52:23', '2023-01-06 14:52:23'),
(98, 'Satya', 0, 24, '2023-01-06 14:52:23', '2023-01-06 14:52:23'),
(99, 'Asteya', 0, 24, '2023-01-06 14:52:23', '2023-01-06 14:52:23'),
(100, 'Brahmacharya', 1, 24, '2023-01-06 14:52:23', '2023-01-06 14:52:23'),
(101, 'Path to attain moksha.', 0, 25, '2023-01-06 14:52:23', '2023-01-06 14:52:23'),
(102, 'Explains the reasons of dukkha as well as the key to its liberation.', 1, 25, '2023-01-06 14:52:23', '2023-01-06 14:52:23'),
(103, 'Gives the account of pancha-khanda', 0, 25, '2023-01-06 14:52:23', '2023-01-06 14:52:23'),
(104, 'Circle of life and death.', 0, 25, '2023-01-06 14:52:23', '2023-01-06 14:52:23'),
(105, 'Susima', 0, 26, '2023-01-06 14:52:23', '2023-01-06 14:52:23'),
(106, 'Kunala', 0, 26, '2023-01-06 14:52:23', '2023-01-06 14:52:23'),
(107, 'Ashoka', 1, 26, '2023-01-06 14:52:23', '2023-01-06 14:52:23'),
(108, 'Vaisya', 0, 26, '2023-01-06 14:52:23', '2023-01-06 14:52:23'),
(109, 'Dharmasthas', 0, 27, '2023-01-06 14:52:23', '2023-01-06 14:52:23'),
(110, 'Pradeshtris', 1, 27, '2023-01-06 14:52:23', '2023-01-06 14:52:23'),
(111, 'Mahamattas', 0, 27, '2023-01-06 14:52:23', '2023-01-06 14:52:23'),
(112, 'None of the above', 0, 27, '2023-01-06 14:52:23', '2023-01-06 14:52:23'),
(113, 'Gupta period', 0, 28, '2023-01-06 14:52:23', '2023-01-06 14:52:23'),
(114, 'Maurya period', 1, 28, '2023-01-06 14:52:23', '2023-01-06 14:52:23'),
(115, 'Vedic period', 0, 28, '2023-01-06 14:52:23', '2023-01-06 14:52:23'),
(116, 'Post-vedic period', 0, 28, '2023-01-06 14:52:23', '2023-01-06 14:52:23'),
(117, '151 BCE', 1, 29, '2023-01-06 14:52:23', '2023-01-06 14:52:23'),
(118, '165 BCE', 0, 29, '2023-01-06 14:52:23', '2023-01-06 14:52:23'),
(119, '180 BCE', 0, 29, '2023-01-06 14:52:23', '2023-01-06 14:52:23'),
(120, '100 AD', 0, 29, '2023-01-06 14:52:23', '2023-01-06 14:52:23'),
(121, 'Shulka', 1, 30, '2023-01-06 14:52:23', '2023-01-06 14:52:23'),
(122, 'Bhaga', 0, 30, '2023-01-06 14:52:23', '2023-01-06 14:52:23'),
(123, 'Bhoga', 0, 30, '2023-01-06 14:52:23', '2023-01-06 14:52:23'),
(124, 'Kara', 0, 30, '2023-01-06 14:52:23', '2023-01-06 14:52:23'),
(125, 'Pravarasena', 1, 31, '2023-01-06 14:52:23', '2023-01-06 14:52:23'),
(126, 'Vindhyashakti', 0, 31, '2023-01-06 14:52:23', '2023-01-06 14:52:23'),
(127, 'Prithvisena', 0, 31, '2023-01-06 14:52:23', '2023-01-06 14:52:23'),
(128, 'Rudrasena', 0, 31, '2023-01-06 14:52:23', '2023-01-06 14:52:23'),
(129, 'Kalidasa', 0, 32, '2023-01-06 14:52:23', '2023-01-06 14:52:23'),
(130, 'Banabhatta', 1, 32, '2023-01-06 14:52:23', '2023-01-06 14:52:23'),
(131, 'Harishena', 0, 32, '2023-01-06 14:52:23', '2023-01-06 14:52:23'),
(132, 'Bhasa', 0, 32, '2023-01-06 14:52:23', '2023-01-06 14:52:23'),
(133, 'Prabhakar Vardhana', 1, 33, '2023-01-06 14:52:23', '2023-01-06 14:52:23'),
(134, 'Harsha Vardhana', 0, 33, '2023-01-06 14:52:23', '2023-01-06 14:52:23'),
(135, 'Rajya Vardhana', 0, 33, '2023-01-06 14:52:23', '2023-01-06 14:52:23'),
(136, 'Grahavarman', 0, 33, '2023-01-06 14:52:23', '2023-01-06 14:52:23'),
(137, 'Vijayaditya', 1, 34, '2023-01-06 14:52:23', '2023-01-06 14:52:23'),
(138, 'Vakpatiraja', 0, 34, '2023-01-06 14:52:23', '2023-01-06 14:52:23'),
(139, 'Manorathavarman', 0, 34, '2023-01-06 14:52:23', '2023-01-06 14:52:23'),
(140, 'Vinayaditya', 0, 34, '2023-01-06 14:52:23', '2023-01-06 14:52:23'),
(141, 'Vatsaraja', 0, 35, '2023-01-06 14:52:23', '2023-01-06 14:52:23'),
(142, 'Nagabhatta I', 0, 35, '2023-01-06 14:52:23', '2023-01-06 14:52:23'),
(143, 'Harichandra', 0, 35, '2023-01-06 14:52:23', '2023-01-06 14:52:23'),
(144, 'Mihir Bhoja', 1, 35, '2023-01-06 14:52:23', '2023-01-06 14:52:23'),
(145, 'Dharmadasagani', 0, 36, '2023-01-06 14:52:23', '2023-01-06 14:52:23'),
(146, 'Nandishena', 1, 36, '2023-01-06 14:52:23', '2023-01-06 14:52:23'),
(147, 'Mahesvarasuri', 0, 36, '2023-01-06 14:52:23', '2023-01-06 14:52:23'),
(148, 'Uddyotanasuri', 0, 36, '2023-01-06 14:52:23', '2023-01-06 14:52:23'),
(149, 'Chalukyas', 1, 37, '2023-01-06 14:52:23', '2023-01-06 14:52:23'),
(150, 'Chahmanas', 0, 37, '2023-01-06 14:52:23', '2023-01-06 14:52:23'),
(151, 'Chandellas', 0, 37, '2023-01-06 14:52:23', '2023-01-06 14:52:23'),
(152, 'Vakatakas', 0, 37, '2023-01-06 14:52:23', '2023-01-06 14:52:23'),
(153, 'Dhruva', 0, 38, '2023-01-06 14:52:23', '2023-01-06 14:52:23'),
(154, 'Govind II', 0, 38, '2023-01-06 14:52:23', '2023-01-06 14:52:23'),
(155, 'Amoghvarsha I', 1, 38, '2023-01-06 14:52:23', '2023-01-06 14:52:23'),
(156, 'Dantidurga', 0, 38, '2023-01-06 14:52:23', '2023-01-06 14:52:23'),
(157, 'Virakurcha', 0, 39, '2023-01-06 14:52:23', '2023-01-06 14:52:23'),
(158, 'Dantivarman', 0, 39, '2023-01-06 14:52:23', '2023-01-06 14:52:23'),
(159, 'Nandivarman I', 0, 39, '2023-01-06 14:52:23', '2023-01-06 14:52:23'),
(160, 'Nandivarman II', 1, 39, '2023-01-06 14:52:23', '2023-01-06 14:52:23'),
(161, 'Manyakheta', 0, 40, '2023-01-06 14:52:23', '2023-01-06 14:52:23'),
(162, 'Kalyanpura', 1, 40, '2023-01-06 14:52:23', '2023-01-06 14:52:23'),
(163, 'Badami', 0, 40, '2023-01-06 14:52:23', '2023-01-06 14:52:23'),
(164, 'Kanchi', 0, 40, '2023-01-06 14:52:23', '2023-01-06 14:52:23'),
(165, 'Virarajendra', 0, 41, '2023-01-06 14:52:23', '2023-01-06 14:52:23'),
(166, 'Rajendra II', 0, 41, '2023-01-06 14:52:23', '2023-01-06 14:52:23'),
(167, 'Rajadhiraja', 1, 41, '2023-01-06 14:52:23', '2023-01-06 14:52:23'),
(168, 'Athirajendra', 0, 41, '2023-01-06 14:52:23', '2023-01-06 14:52:23'),
(169, 'Arikesary Maravarman', 0, 42, '2023-01-06 14:52:23', '2023-01-06 14:52:23'),
(170, 'Maravarman Rajasimha I', 1, 42, '2023-01-06 14:52:23', '2023-01-06 14:52:23'),
(171, 'Varguna I', 0, 42, '2023-01-06 14:52:23', '2023-01-06 14:52:23'),
(172, 'Nedum-Cheliyan', 0, 42, '2023-01-06 14:52:23', '2023-01-06 14:52:23'),
(173, 'Madurai', 1, 43, '2023-01-06 14:52:23', '2023-01-06 14:52:23'),
(174, 'Thenmadurai', 0, 43, '2023-01-06 14:52:23', '2023-01-06 14:52:23'),
(175, 'Kabadapuram', 0, 43, '2023-01-06 14:52:23', '2023-01-06 14:52:23'),
(176, 'Vellore', 0, 43, '2023-01-06 14:52:23', '2023-01-06 14:52:23'),
(177, 'First sangam', 0, 44, '2023-01-06 14:52:23', '2023-01-06 14:52:23'),
(178, 'Second sangam', 0, 44, '2023-01-06 14:52:24', '2023-01-06 14:52:24'),
(179, 'Third sangam', 1, 44, '2023-01-06 14:52:24', '2023-01-06 14:52:24'),
(180, 'None of the above', 0, 44, '2023-01-06 14:52:24', '2023-01-06 14:52:24'),
(181, 'Only 1 & 2', 0, 45, '2023-01-06 14:52:24', '2023-01-06 14:52:24'),
(182, 'Only 2 & 3', 0, 45, '2023-01-06 14:52:24', '2023-01-06 14:52:24'),
(183, 'Only 2, 3 & 4', 0, 45, '2023-01-06 14:52:24', '2023-01-06 14:52:24'),
(184, '1, 2, 3 & 4', 1, 45, '2023-01-06 14:52:24', '2023-01-06 14:52:24'),
(185, 'Mohenjo-Daro', 0, 46, '2023-01-06 14:52:24', '2023-01-06 14:52:24'),
(186, 'Lothal', 0, 46, '2023-01-06 14:52:24', '2023-01-06 14:52:24'),
(187, 'Surkotada', 1, 46, '2023-01-06 14:52:24', '2023-01-06 14:52:24'),
(188, 'Suktagen Dor', 0, 46, '2023-01-06 14:52:24', '2023-01-06 14:52:24'),
(189, 'Maharashtra', 0, 47, '2023-01-06 14:52:24', '2023-01-06 14:52:24'),
(190, 'Tamil Nadu', 1, 47, '2023-01-06 14:52:24', '2023-01-06 14:52:24'),
(191, 'Kerala', 0, 47, '2023-01-06 14:52:24', '2023-01-06 14:52:24'),
(192, 'Karnataka', 0, 47, '2023-01-06 14:52:24', '2023-01-06 14:52:24'),
(193, 'Rigveda', 0, 48, '2023-01-06 14:52:24', '2023-01-06 14:52:24'),
(194, 'Vishnu Purana', 1, 48, '2023-01-06 14:52:24', '2023-01-06 14:52:24'),
(195, 'Samveda', 0, 48, '2023-01-06 14:52:24', '2023-01-06 14:52:24'),
(196, 'Yajurveda', 0, 48, '2023-01-06 14:52:24', '2023-01-06 14:52:24'),
(197, 'Jharkhand', 0, 49, '2023-01-06 14:52:24', '2023-01-06 14:52:24'),
(198, 'Andhra Pradesh', 0, 49, '2023-01-06 14:52:24', '2023-01-06 14:52:24'),
(199, 'Arunachal Pradesh', 0, 49, '2023-01-06 14:52:24', '2023-01-06 14:52:24'),
(200, 'Bihar', 1, 49, '2023-01-06 14:52:24', '2023-01-06 14:52:24'),
(201, 'Test', 1, 50, '2023-01-26 12:55:27', '2023-01-26 12:55:27'),
(202, 'Test 3', 0, 50, '2023-01-26 12:55:27', '2023-01-26 12:55:27'),
(203, 'Test', 1, 51, '2023-01-26 13:03:14', '2023-01-26 13:03:14'),
(204, 'Test 3', 0, 51, '2023-01-26 13:03:14', '2023-01-26 13:03:14'),
(205, 'Test', 1, 52, '2023-01-26 13:13:32', '2023-01-26 13:13:32'),
(206, 'Test 3', 0, 52, '2023-01-26 13:13:32', '2023-01-26 13:13:32'),
(207, 'Test', 1, 53, '2023-01-26 13:15:23', '2023-01-26 13:15:23'),
(208, 'Test 3', 0, 53, '2023-01-26 13:15:23', '2023-01-26 13:15:23'),
(209, 'Test', 1, 54, '2023-01-26 13:15:40', '2023-01-26 13:15:40'),
(210, 'Test 3', 0, 54, '2023-01-26 13:15:40', '2023-01-26 13:15:40'),
(211, 'Test', 1, 55, '2023-01-26 13:19:35', '2023-01-26 13:19:35'),
(212, 'Test 3', 0, 55, '2023-01-26 13:19:35', '2023-01-26 13:19:35'),
(213, 'Test', 1, 56, '2023-01-26 13:28:38', '2023-01-26 13:28:38'),
(214, 'Test 3', 0, 56, '2023-01-26 13:28:38', '2023-01-26 13:28:38'),
(215, 'Test', 1, 57, '2023-01-26 13:29:00', '2023-01-26 13:29:00'),
(216, 'Test 3', 0, 57, '2023-01-26 13:29:00', '2023-01-26 13:29:00'),
(217, 'Test', 1, 58, '2023-01-26 13:53:59', '2023-01-26 13:53:59'),
(218, 'Test 3', 0, 58, '2023-01-26 13:53:59', '2023-01-26 13:53:59'),
(219, 'Test', 1, 59, '2023-01-28 02:35:32', '2023-01-28 02:35:32'),
(220, 'Test 3', 0, 59, '2023-01-28 02:35:32', '2023-01-28 02:35:32'),
(221, 'Test', 1, 60, '2023-01-28 22:50:40', '2023-01-28 22:50:40'),
(222, 'Test 3', 0, 60, '2023-01-28 22:50:40', '2023-01-28 22:50:40');

-- --------------------------------------------------------

--
-- Table structure for table `process_chats`
--

CREATE TABLE `process_chats` (
  `id` bigint UNSIGNED NOT NULL,
  `chat_id` bigint UNSIGNED NOT NULL,
  `event_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `row_id` bigint UNSIGNED DEFAULT NULL,
  `value` text COLLATE utf8mb4_unicode_ci,
  `is_done` enum('1','2','3') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1' COMMENT '1->draft,2->inProgress,3=>complete',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `process_chats`
--

INSERT INTO `process_chats` (`id`, `chat_id`, `event_name`, `row_id`, `value`, `is_done`, `created_at`, `updated_at`) VALUES
(1, 2067178683, 'quiz_add', 12, 'QUIZ 1', '3', '2023-01-24 22:03:39', '2023-01-26 04:41:30'),
(7, 2067178683, 'quiz_add_description', 12, 'DESCRIPTION', '3', '2023-01-26 04:41:30', '2023-01-26 04:41:46'),
(8, 2067178683, 'quiz_add_rule', 12, 'RULE', '3', '2023-01-26 04:41:46', '2023-01-26 04:43:58'),
(9, 2067178683, 'quiz_add_question', 12, NULL, '1', '2023-01-26 04:43:58', '2023-01-26 04:43:58'),
(10, 2067178683, 'quiz_add', NULL, NULL, '1', '2023-01-28 02:07:33', '2023-01-28 02:07:33'),
(11, 2067178683, 'quiz_add', NULL, NULL, '1', '2023-01-28 02:16:21', '2023-01-28 02:16:21'),
(12, 2067178683, 'quiz_add', NULL, NULL, '1', '2023-01-28 02:20:27', '2023-01-28 02:20:27'),
(13, 2067178683, 'quiz_add', NULL, NULL, '1', '2023-01-28 02:22:50', '2023-01-28 02:22:50'),
(14, 2067178683, 'quiz_add', NULL, NULL, '1', '2023-01-28 02:28:24', '2023-01-28 02:28:24'),
(15, 2067178683, 'quiz_add', 13, 'RULE', '3', '2023-01-28 02:28:46', '2023-01-28 02:29:46'),
(16, 2067178683, 'quiz_add_description', 13, 'RULE', '3', '2023-01-28 02:29:46', '2023-01-28 02:30:14'),
(17, 2067178683, 'quiz_add_rule', 13, 'RULE', '3', '2023-01-28 02:30:14', '2023-01-28 02:30:20'),
(18, 2067178683, 'quiz_add_question', 13, NULL, '1', '2023-01-28 02:30:20', '2023-01-28 02:30:20'),
(19, 2067178683, 'quiz_done', NULL, NULL, '1', '2023-01-28 02:35:52', '2023-01-28 02:35:52'),
(20, 2067178683, 'quiz_add', 14, 'QUIZ 2', '3', '2023-01-28 22:25:36', '2023-01-28 22:43:18'),
(21, 2067178683, 'quiz_add_description', 14, 'DESCRIPTION', '3', '2023-01-28 22:43:18', '2023-01-28 22:44:00'),
(22, 2067178683, 'quiz_add_rule', 14, 'RULE', '3', '2023-01-28 22:44:00', '2023-01-28 22:46:00'),
(23, 2067178683, 'quiz_add_question', 14, NULL, '1', '2023-01-28 22:46:00', '2023-01-28 22:46:00'),
(24, 2067178683, 'quiz_done', NULL, NULL, '1', '2023-01-28 22:51:27', '2023-01-28 22:51:27');

-- --------------------------------------------------------

--
-- Table structure for table `questions`
--

CREATE TABLE `questions` (
  `id` bigint UNSIGNED NOT NULL,
  `text` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `isMultiple` tinyint(1) NOT NULL DEFAULT '0',
  `time` int DEFAULT NULL,
  `status` enum('1','2') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `questions`
--

INSERT INTO `questions` (`id`, `text`, `isMultiple`, `time`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Which of the following term is used for a “school” of learning and teaching the branches of Vedas?', 0, NULL, '1', '2023-01-06 14:52:22', '2023-01-06 14:52:22'),
(2, 'Which among conclusions has been derived from the debasement of the coins and gradual disappearance of gold coins during the post-Gupta period?', 0, NULL, '1', '2023-01-06 14:52:22', '2023-01-06 14:52:22'),
(3, 'Which among the following plays was / were written by Harsha?', 0, NULL, '1', '2023-01-06 14:52:22', '2023-01-06 14:52:22'),
(4, 'Who was the author of Aihole Inscription ?', 0, NULL, '1', '2023-01-06 14:52:22', '2023-01-06 14:52:22'),
(5, 'Who was the founder of Nanda dynasty?', 0, NULL, '1', '2023-01-06 14:52:22', '2023-01-06 14:52:22'),
(6, 'Who granted permission to Buddhist king of Ceylon Meghavarman to build a monastic at Bodh Gaya?', 0, NULL, '1', '2023-01-06 14:52:22', '2023-01-06 14:52:22'),
(7, 'Rummindei Pillar Inscription which talks about taxation in Maurya era has been found at which of the following places?', 0, NULL, '1', '2023-01-06 14:52:22', '2023-01-06 14:52:22'),
(8, 'Which among the following Buddhist Canon is related to dealing with rules for monks and nuns ?', 0, NULL, '1', '2023-01-06 14:52:22', '2023-01-06 14:52:22'),
(9, 'The following were the immediate successors of imperial Mauryas?', 0, NULL, '1', '2023-01-06 14:52:22', '2023-01-06 14:52:22'),
(10, 'Giak & Kiari are located in which of the following ?', 0, NULL, '1', '2023-01-06 14:52:22', '2023-01-06 14:52:22'),
(11, 'The harappans used intaglio seals, made mostly of carved and fired steatite :', 0, NULL, '1', '2023-01-06 14:52:22', '2023-01-06 14:52:22'),
(12, 'Consider the following differences between North and South temple architecture:', 0, NULL, '1', '2023-01-06 14:52:22', '2023-01-06 14:52:22'),
(13, 'Which of the following physicians was a contemporary of Gautama Buddha ?', 0, NULL, '1', '2023-01-06 14:52:22', '2023-01-06 14:52:22'),
(14, 'With reference to the Aranyakas, which among the following statements is / are correct?', 0, NULL, '1', '2023-01-06 14:52:22', '2023-01-06 14:52:22'),
(15, 'Which of the following find their origin in the Gupta Era?', 0, NULL, '1', '2023-01-06 14:52:22', '2023-01-06 14:52:22'),
(16, 'Which among the following is / are features of the Post-Gupta society of India?', 0, NULL, '1', '2023-01-06 14:52:22', '2023-01-06 14:52:22'),
(17, 'Consider the following:', 0, NULL, '1', '2023-01-06 14:52:22', '2023-01-06 14:52:22'),
(18, 'Consider the following pairs of Harappan sites with their burial customs:', 0, NULL, '1', '2023-01-06 14:52:22', '2023-01-06 14:52:22'),
(19, 'The terms Swarajya and Vairajya were used in Later Vedic Period to denote which form of government?', 0, NULL, '1', '2023-01-06 14:52:22', '2023-01-06 14:52:22'),
(20, 'In which among the following Mahajanapada, Lord Buddha attained parinirvana?', 0, NULL, '1', '2023-01-06 14:52:23', '2023-01-06 14:52:23'),
(21, 'Which of the following site is known for cave paintings of Mesolithic Age?', 0, NULL, '1', '2023-01-06 14:52:23', '2023-01-06 14:52:23'),
(22, 'What was the name given to the head of the family during the Rig Vedic Age?', 0, NULL, '1', '2023-01-06 14:52:23', '2023-01-06 14:52:23'),
(23, 'Which of the following are correct regarding the Rig Vedic age?', 0, NULL, '1', '2023-01-06 14:52:23', '2023-01-06 14:52:23'),
(24, 'Which of the following doctrine is added by Jainism?', 0, NULL, '1', '2023-01-06 14:52:23', '2023-01-06 14:52:23'),
(25, 'The law of dependent organization or patichcha-samuppada refers to', 0, NULL, '1', '2023-01-06 14:52:23', '2023-01-06 14:52:23'),
(26, 'Who was appointed as the viceroy of Taxila and Ujjain during the reign of Bindusara?', 0, NULL, '1', '2023-01-06 14:52:23', '2023-01-06 14:52:23'),
(27, 'Which of the following were the officers responsible for the suppression of criminals?', 0, NULL, '1', '2023-01-06 14:52:23', '2023-01-06 14:52:23'),
(28, 'Which of the following period saw the emergence of rock-cut architecture?', 0, NULL, '1', '2023-01-06 14:52:23', '2023-01-06 14:52:23'),
(29, 'Pushyamitra Shunga died in which of the following years?', 0, NULL, '1', '2023-01-06 14:52:23', '2023-01-06 14:52:23'),
(30, 'Which of the following refers to the royal share of merchandise brought into a town or harbour by merchants?', 0, NULL, '1', '2023-01-06 14:52:23', '2023-01-06 14:52:23'),
(31, 'Which of the following Vakataka king took the title of Samrat?', 0, NULL, '1', '2023-01-06 14:52:23', '2023-01-06 14:52:23'),
(32, 'Which of the following was the court poet of Harsha?', 0, NULL, '1', '2023-01-06 14:52:23', '2023-01-06 14:52:23'),
(33, 'Which of the following  actually laid the foundations of the Pushyabhuti dynasty?', 0, NULL, '1', '2023-01-06 14:52:23', '2023-01-06 14:52:23'),
(34, 'Which of the following Chalukya king fought with the Yasovarman?', 0, NULL, '1', '2023-01-06 14:52:23', '2023-01-06 14:52:23'),
(35, 'Which of the following kings was identified as king Juzr from the travel accounts of the 9th-century Arab merchant, Sulaiman?', 0, NULL, '1', '2023-01-06 14:52:23', '2023-01-06 14:52:23'),
(36, 'Who was the author of Ajita-Santi-Stava?', 0, NULL, '1', '2023-01-06 14:52:23', '2023-01-06 14:52:23'),
(37, 'Rashtrakutas are believed to the feduatories of which of the following?', 0, NULL, '1', '2023-01-06 14:52:23', '2023-01-06 14:52:23'),
(38, 'Which of the following kings wrote Kavirajamarga?', 0, NULL, '1', '2023-01-06 14:52:23', '2023-01-06 14:52:23'),
(39, 'Paramesvaravarman II was succeeded by which of the following kings?', 0, NULL, '1', '2023-01-06 14:52:23', '2023-01-06 14:52:23'),
(40, 'Somesvara I established his capital at which of the following places?', 0, NULL, '1', '2023-01-06 14:52:23', '2023-01-06 14:52:23'),
(41, 'Which of the following kings succeeded Rajendra I?', 0, NULL, '1', '2023-01-06 14:52:23', '2023-01-06 14:52:23'),
(42, 'Which of the following kings succeeded the king Koccadiyan Ranadhira?', 0, NULL, '1', '2023-01-06 14:52:23', '2023-01-06 14:52:23'),
(43, 'Where was the third Sangam held?', 0, NULL, '1', '2023-01-06 14:52:23', '2023-01-06 14:52:23'),
(44, 'Nedumthokai, Kurumthokai, Nattinai, Ainkurunnu, Pathittupattu, Paripadla, Kuttu and Vari are the works of which of the following sangams?', 0, NULL, '1', '2023-01-06 14:52:23', '2023-01-06 14:52:23'),
(45, 'Which of the following were the major items of export to the Romans during the Sangam period?', 0, NULL, '1', '2023-01-06 14:52:24', '2023-01-06 14:52:24'),
(46, 'Remains of horse have been found from which site of Indus Valley Civilization?', 0, NULL, '1', '2023-01-06 14:52:24', '2023-01-06 14:52:24'),
(47, 'Which among these modern States did the Alwar saints originate from?', 0, NULL, '1', '2023-01-06 14:52:24', '2023-01-06 14:52:24'),
(48, 'Which of these is not a part of Veda?', 0, NULL, '1', '2023-01-06 14:52:24', '2023-01-06 14:52:24'),
(49, 'With the goal of promoting and assisting the MSME sector, SIDBI has partnered with which state government?', 0, NULL, '1', '2023-01-06 14:52:24', '2023-01-06 14:52:24'),
(50, 'Quiz 1', 0, NULL, NULL, '2023-01-26 12:55:27', '2023-01-26 12:55:27'),
(51, 'Quiz 1', 0, NULL, NULL, '2023-01-26 13:03:14', '2023-01-26 13:03:14'),
(52, 'Quiz 1', 0, NULL, NULL, '2023-01-26 13:13:32', '2023-01-26 13:13:32'),
(53, 'Quiz 1', 0, NULL, NULL, '2023-01-26 13:15:23', '2023-01-26 13:15:23'),
(54, 'Quiz 1', 0, NULL, NULL, '2023-01-26 13:15:40', '2023-01-26 13:15:40'),
(55, 'Quiz 1', 0, NULL, NULL, '2023-01-26 13:19:35', '2023-01-26 13:19:35'),
(56, 'Quiz 1', 0, NULL, NULL, '2023-01-26 13:28:38', '2023-01-26 13:28:38'),
(57, 'Quiz 1', 0, NULL, NULL, '2023-01-26 13:29:00', '2023-01-26 13:29:00'),
(58, 'Quiz 1', 0, NULL, NULL, '2023-01-26 13:53:59', '2023-01-26 13:53:59'),
(59, 'Quiz 1', 0, NULL, NULL, '2023-01-28 02:35:32', '2023-01-28 02:35:32'),
(60, 'Quiz 1', 0, NULL, NULL, '2023-01-28 22:50:40', '2023-01-28 22:50:40');

-- --------------------------------------------------------

--
-- Table structure for table `quizzes`
--

CREATE TABLE `quizzes` (
  `id` bigint UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `rules` text COLLATE utf8mb4_unicode_ci,
  `status` enum('1','2') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '2',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `quizzes`
--

INSERT INTO `quizzes` (`id`, `name`, `description`, `rules`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Ancient Indian History Quiz Multiple Choice Questions (MCQs)', 'Ancient Indian History Quiz Multiple Choice Questions (MCQs)', 'Ancient Indian History Quiz Multiple Choice Questions (MCQs)', '2', '2023-01-06 14:52:22', '2023-01-06 14:52:22'),
(12, 'QUIZ 1', 'DESCRIPTION', 'RULE', '2', '2023-01-26 04:41:29', '2023-01-26 04:43:57'),
(13, 'RULE', 'RULE', 'RULE', '2', '2023-01-28 02:29:45', '2023-01-28 02:30:19'),
(14, 'QUIZ 2', 'DESCRIPTION', 'RULE', '2', '2023-01-28 22:43:17', '2023-01-28 22:45:59');

-- --------------------------------------------------------

--
-- Table structure for table `quiz_codes`
--

CREATE TABLE `quiz_codes` (
  `id` bigint UNSIGNED NOT NULL,
  `quiz_id` bigint UNSIGNED NOT NULL,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telegram_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `quiz_codes`
--

INSERT INTO `quiz_codes` (`id`, `quiz_id`, `code`, `telegram_id`, `created_at`, `updated_at`) VALUES
(1, 1, 'code1', NULL, NULL, NULL),
(4, 12, '63d2c9d49fe03', '2067178683', '2023-01-26 13:13:32', '2023-01-26 13:13:32'),
(10, 13, '63d4d74c3376b', '2067178683', '2023-01-28 02:35:32', '2023-01-28 02:35:32'),
(11, 14, '63d5f4190110e', '2067178683', '2023-01-28 22:50:41', '2023-01-28 22:50:41');

-- --------------------------------------------------------

--
-- Table structure for table `quiz_questions`
--

CREATE TABLE `quiz_questions` (
  `id` bigint UNSIGNED NOT NULL,
  `question_id` bigint UNSIGNED NOT NULL,
  `quiz_id` bigint UNSIGNED NOT NULL,
  `notes` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `expire_time` int DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `quiz_questions`
--

INSERT INTO `quiz_questions` (`id`, `question_id`, `quiz_id`, `notes`, `expire_time`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'Charana refers to the Guru-pupil lineage or school for teaching and learning of Vedas in ancient India.', NULL, '2023-01-06 14:52:22', '2023-01-06 14:52:22'),
(2, 2, 1, 'Debasement of the coins and gradual disappearance of gold coins during the post-Gupta period indicates the Decline of Trade.', NULL, '2023-01-06 14:52:22', '2023-01-06 14:52:22'),
(3, 3, 1, 'Harsha wrote three sanskrit plays- Nagananda, Ratnavali and Priyadarshika.', NULL, '2023-01-06 14:52:22', '2023-01-06 14:52:22'),
(4, 4, 1, 'Aihole Inscription is a eulogy written by Ravikirti who was the court poet of Chalukya King Pulakesin II.', NULL, '2023-01-06 14:52:22', '2023-01-06 14:52:22'),
(5, 5, 1, 'Mahapadmananda (345 BC – 329 BC) was the founder of Nanda dynasty. Mahapadmananda was also known as Ekarat and Sarvakshatrantaka.', NULL, '2023-01-06 14:52:22', '2023-01-06 14:52:22'),
(6, 6, 1, 'The Gupta emperor, Samudragupta granted permission to Buddhist king of Ceylon Meghavarman to build a monastry at Bodh Gaya. Hence, he was also known as Anukampavan (full of compassion).', NULL, '2023-01-06 14:52:22', '2023-01-06 14:52:22'),
(7, 7, 1, 'Lumbini Pillar Edict in Nepal is known as the Rummindei Pillar Inscription .The Lumbini Pillar Edict recorded that sometime after the twentieth year of his reign, Ashoka travelled to the Buddha’s birthplace and personally made offerings. He then had a stone pillar set up and reduced the taxes of the people in that area.', NULL, '2023-01-06 14:52:22', '2023-01-06 14:52:22'),
(8, 8, 1, 'Tripitaka or Three Baskets is a traditional term used for various Buddhist scriptures. It is known as pali Canon in English. The three pitakas are Sutta Pitaka, Vinaya Pitaka and Abhidhamma Pitaka. Sutta Pitaka: It contains over 10 thousand suttas or sutras related to Buddha and his close companions. This also deals with the first Buddhist council which was held shortly after Buddha’s death, dated by the majority of recent scholars around 400 BC, under the patronage of king Ajatasatru with the monk Mahakasyapa presiding, at Rajgir. It is divided into various sections as shown in following graphics: Vinaya Pitaka: The subject matter of Vinay Pitaka is the monastic rules for monks and nuns. It can also be called as Book of Discipline. Its three books are Suttavibhanga, Khandaka and Parivara. Abhidhammapitaka deals with the philosophy and doctrine of Buddhism appearing in the suttas. However, it does not contain the systematic philosophical treatises. There are 7 works of Abhidhamma Pitaka which most scholars agree that don’t represent the words of Buddha himself.', NULL, '2023-01-06 14:52:22', '2023-01-06 14:52:22'),
(9, 9, 1, 'The Shunga Empire was an ancient Indian dynasty from Magadha that controlled areas of the central and eastern Indian subcontinent from around 185 to 75 BCE. They are the immediate successors of imperial Mauryas.', NULL, '2023-01-06 14:52:22', '2023-01-06 14:52:22'),
(10, 10, 1, 'Giak & Kiari are located in Ladakh. They are examples of Neolithic sites', NULL, '2023-01-06 14:52:22', '2023-01-06 14:52:22'),
(11, 11, 1, 'The seals of the ancient Harappan’s were probably used in much the same way they are today, to sign letters or for commercial transactions. The Harappan seals were used for marking goods and bales of merchandise. (Hence Option d. is correct)', NULL, '2023-01-06 14:52:22', '2023-01-06 14:52:22'),
(12, 12, 1, 'Both are correct statements.  The North Indian temples follow the Ngara style of architecture while the south Indian temples follow the Dravidian architecture. Shikhara is in Nagara style and Vimana is Davidian style.', NULL, '2023-01-06 14:52:22', '2023-01-06 14:52:22'),
(13, 13, 1, 'Jivaka was the Buddha’s personal physician and is considered as the father of Buddhist medicine.', NULL, '2023-01-06 14:52:22', '2023-01-06 14:52:22'),
(14, 14, 1, 'The Aranyakas were written in Forests and are concluding parts of the Brahmans. Aranyakas don’t lay much emphasis on rites, ritual and sacrifices but have philosophy and mysticism. So they have moral science and philosophy. It also provides the details of the rishis who lived in jungles. The end portions of many Brahmanas have an esoteric content, called the ‘Aranyakas’', NULL, '2023-01-06 14:52:22', '2023-01-06 14:52:22'),
(15, 15, 1, 'Mahayana Buddhism, also known as the Great Vehicle, is the form of Buddhism prominent in North Asia, including China, Mongolia, Tibet, Korea, and Japan. It started in the first century C.E. and Vedanta school is of much earlier time period.', NULL, '2023-01-06 14:52:22', '2023-01-06 14:52:22'),
(16, 16, 1, 'The summary of the societal conditions in early medieval India are as follows: Political decentralization The new polity is characterized by decentralization and hierarchy, features suggested by the presence of a wide range of semi-autonomous rulers, Samantas, Mahasamantas and others and the hierarchized positioning of numerous Rajapurushas employed by royal courts. Emergence of Landed intermediaries This is the hallmark of Indian feudal social formation and is seen to be linked both to the disintegration and decentralization of state authority and to major changes in the structure of agrarian relations. The emergence of landed intermediaries- a dominant landholding social group absent in the early historical period- is linked to the practice of land grants which began with the Satavahanas. The earliest land grants belonging to the first century BC were given to the Buddhist priests and Brahmans and other religious establishments. However, in the post- gupta period even administrative officials were granted land. The landed beneficiaries were given both powers of taxation and coercion, leading to the disintegration of the central authority. The secular recipients of the grants and the autonomous holders of land are generally termed as fief holders and free holders. Localization of economy There was a economy to self-sufficient villages as units of production. Thus, ruralisation was an important dimension of the transition process. This change was the result of the decline of early historical urban centres and commercial networks leading to the practice of payment in land grants instead of earlier practice of remuneration in cash, migration of different urban social groups to rural areas, expansion of agrarian space and the crystallization of Jajmani type of relationships in the rural areas. According to one formulation, fief holders and free-holders in rural society emerged as agents of social change in the later phase of early medieval society, generating once again such features of early historical economy as trade, urbanization and a market economy. Subjection of the peasantry Likened sometimes to serfdom, characterizing the of the subjection of peasantry, such as immobility, forced labour (vishti) and the payment of revenue at exorbitantly high rates- all point to the nature of stratification in Post-Gupta society. The condition of the peasantry in this pattern of rural stratification was in sharp contrast to what the agrarian structure in early historical India represented, since that structure was dominated by free vaishya peasants and labour services provided by the Shudras. It is important to note here that although the earliest example of sharecroppers being transferred along with the land can be traced in the third century pallava inscription from Andhra, Orissa and Deccan, from the sixth century AD onwards sharecroppers and peasants were particularly asked to stick to the land granted to the beneficiaries. The custom became fairly common in the post-gupta period and the villages transferred to the grantees are known as dhana-jana-sahita, janata-samriddha and saprativasi-jana- sameta. Thus the artisans and peasants were asked not to leave the village granted to the beneficiaries or migrate to tax-free village. Proliferation of castes A striking social development from about the seventh century onwards was the proliferation of castes. The Brahmavaivarta Purana, a seventh century work, counts 100 castes including 61 castes noted by Manu, but the Vishnudharmottara Purana (8th century) states that thousands of mixed castes are produced by the connection of vaishyas women with men of lower castes. In fact, proliferation affected the brahmanas, the Rajputs, and above all, the shudras and untouchables. Increasing pride of birth, characteristic of feudal society, and the accompanying self-sufficient village economy, which prevented both spatial and occupational mobility, gave rise to many castes. The guilds of artisans gradually hardened into castes due to lack of mobility in post-gupta times. The absorption of the tribal peoples into the brahmanical fold, though as old as vedic times, was mainly based on conquests. Coupled with the process of large-scale religious land grants. Acculturation assumed enormous dimensions and considerably added to the variety of the shudras and so-called mixed castes. According to Prof. R.S. Sharma social changes were mainly the product of certain economic developments, such as land grants and large scale transfers of land revenues and land to both secular and religious elements, decline of trade and commerce, loss of mobility of artisans, peasants and traders, unequal distribution of land an power etc. He holds the economic factor responsible for the emergence of certain new castes and decline of certain old ones. Thus the constant transfer of land of land revenues made by princes to priests, temples and officials led to the rise and growth of the scribe or the Kayastha community which undermined the monopoly of Brahmans as writers and scribes. Similarly, the decline of trade and commerce led to the decline in the position of the Vaishyas. The process of proliferation and multiplication of castes was yet another marked feature of the social life of the period.', NULL, '2023-01-06 14:52:22', '2023-01-06 14:52:22'),
(17, 17, 1, 'The important sites of Ahar Culture were Aahar (Rajasthan), Balathal, Gilund etc. Kayatha Culture was mainly located near Chambal and its tributaries. Malwa Culture was distributed on the banks of Narmada & its tributaries in Gujarat.', NULL, '2023-01-06 14:52:22', '2023-01-06 14:52:22'),
(18, 18, 1, 'Important burial sites of Indus Valley Civilization are Harappa, Kalibangan, Rakhigarhi, Lothal, Rojdi, and Ropar. Lothal is one of the most prominent cities of the ancient Indus valley civilization. Evidence of double burial (burying a male and a female in a single grave) has been found here. The most common method of burial was found in Kalibangan. It was to place the body of the deceased in an extended position, with the head towards the north, in a simple pit or brick chamber. Evidence of pot-burial has been found in Surkotada.', NULL, '2023-01-06 14:52:22', '2023-01-06 14:52:22'),
(19, 19, 1, 'The terms \"swarajya\" and \"vairajya\" were used for republic government. The place where only a particular person used to rule mainly was called \"Rajya\" (state).', NULL, '2023-01-06 14:52:22', '2023-01-06 14:52:22'),
(20, 17, 1, 'Tathagat is a Pali and Sanskrit word; Gautama Buddha uses it when referring to himself. Tathagat is one who attained the truth. Syadvada is associated to Jainism. It states that all judgments are conditional, holding good only in certain conditions & circumstances. Upasaka (masculine) and Upasikas (feminine) were the titles given to followers of the Buddha who undertook certain vows, but were not monks, nuns, or monastics. Gautama Buddha left his family to become a ascetic in search of truth. This is called “the great departure\' or Mahabhinishkramana.', NULL, '2023-01-06 14:52:23', '2023-01-06 14:52:23'),
(21, 20, 1, 'Buddha attained Niravana at Kushinagara, a village some 180 Kilometers from Varanasi and it was in the state of Mallas.', NULL, '2023-01-06 14:52:23', '2023-01-06 14:52:23'),
(22, 21, 1, 'The period between 12,000 years ago till about 10,000 years ago is called Mesolithic. In Greek ‘meso’ means the middle and ‘lithic’ means stone. Hence, the Mesolithic stage of prehistory is also known as the Middle Stone Age. It was the transitional phase between the Palaeolithic and the Neolithic Ages Bhimbetka caves which are located in Madhya Pradesh is known for Excellent cave paintings of Mesolithic Age', NULL, '2023-01-06 14:52:23', '2023-01-06 14:52:23'),
(23, 22, 1, 'All the social units during the Rig Vedic Age were based on brotherhood. The Kula or the Family was the basic social unit. The head of the family was known as Kulapa. The Rig Vedic Society followed patrilineal system.', NULL, '2023-01-06 14:52:23', '2023-01-06 14:52:23'),
(24, 23, 1, 'The Rig Vedic Aryans used horses, chariots, better arms made of bronze. They also introduced spoked wheel. They were acquainted with sowing, harvesting and threshing.', NULL, '2023-01-06 14:52:23', '2023-01-06 14:52:23'),
(25, 24, 1, 'Th fifth doctrine of Jainism- celibacy/chastity (brahmacharya) was added by Mahavira. Brahmacharya means total abstinence from sensual pleasure. It is a form of infatuating force, which sets aside all virtues and reason at the time of indulgence.', NULL, '2023-01-06 14:52:23', '2023-01-06 14:52:23'),
(26, 25, 1, 'The law of dependent organization or patichcha-samuppada as mentioned in Buddhism explains the reason of all dukkha, as well as the key to its liberation. There are 12 components arranged in a wheel and one leading to next.', NULL, '2023-01-06 14:52:23', '2023-01-06 14:52:23'),
(27, 26, 1, 'Ashoka was appointed the viceroy of both Taxila and Ujjain during the reign of Mauryan King Bindusara. Both the cities handled commercial activities. Asoka had himself formally crowned in 268 BC.', NULL, '2023-01-06 14:52:23', '2023-01-06 14:52:23'),
(28, 27, 1, 'According to Ashokan inscriptions Pradeshtris were the officers responsible for the suppression of criminals in the Mauryan empire. During that period various civil as well as criminal courts functioned at the local level right from village to province.', NULL, '2023-01-06 14:52:23', '2023-01-06 14:52:23'),
(29, 28, 1, 'Stone culture dramatically emerged as the principal medium of Indian arts in the Mauryan period. Many art forms such as stone sculptures, ring stones, disc stones, terracotta figurines, and stupa architecture were patronized by different Mauryan kings. The Maurya period also saw the beginning of rock-cut architecture.', NULL, '2023-01-06 14:52:23', '2023-01-06 14:52:23'),
(30, 29, 1, 'According to Puranas, Pushyamitra Shunga who founded the Shunga dynasty in 185 or 186 BCE, died in the year 151 BCE. His reign is believed to have lasted for around 36 years.', NULL, '2023-01-06 14:52:23', '2023-01-06 14:52:23'),
(31, 30, 1, 'Shulka during the Gupta period was a royal share of merchandise which was brought into a town or harbour by merchants. Therefore it can also be equated with the customs and tolls.', NULL, '2023-01-06 14:52:23', '2023-01-06 14:52:23'),
(32, 31, 1, 'Pravarasena was the second ruler of the Vakataka Dynasty. He performed an asvamedha sacrifice and a vajapeya sacrifice. He was the only Vakataka king who took the title of Samrat.', NULL, '2023-01-06 14:52:23', '2023-01-06 14:52:23'),
(33, 32, 1, 'Harshacharita was wriiten by Banabhatta. It is the biography of Harsha. Bana was a Brahman. He was the court poet of Harsha. Harshacharita conatins the history of the reign of Harsha.', NULL, '2023-01-06 14:52:23', '2023-01-06 14:52:23'),
(34, 33, 1, 'Prabhakar Vardhana who ruled around mid-6th century CE was the fourth king of the Pushyabhuti dynasty. He actually laid the foundations of the Pushyabhuti dynasty. Prabhakar Vardhana was considered a great general with many military victories.', NULL, '2023-01-06 14:52:23', '2023-01-06 14:52:23'),
(35, 34, 1, 'According to some inscriptions, during the reign of Chalukya king Vinayaditya who ruled from 681 A.D. to 696 A.D. of Badami, his son Vijayaditya fought with the “lord of the whole of northern regions” Yasovarman.', NULL, '2023-01-06 14:52:23', '2023-01-06 14:52:23'),
(36, 35, 1, 'Mihir Bhoja is also identified as the king Juzr from the travel accounts of the 9th-century Arab merchant, Sulaiman. According to the Arab merchant Sulaiman, Mihir Bhoja possessed great military power and riches.', NULL, '2023-01-06 14:52:23', '2023-01-06 14:52:23'),
(37, 36, 1, 'Nandishena was the author of Ajita-Santi-Stava. Kuvalayamala was written by Uddyotanasuri. Dharmadasagani wrote Updesmala and  Mahesvarasuri composed a religious tale called Jnanapanchami-Katha.', NULL, '2023-01-06 14:52:23', '2023-01-06 14:52:23'),
(38, 37, 1, 'The Rashtrakutas are presumed to be a feudatory of the Chalukyas. They rose to power in Deccan between c.753 and 975 CE. Rashtrakuta means the chief of a rashtra.', NULL, '2023-01-06 14:52:23', '2023-01-06 14:52:23'),
(39, 38, 1, 'Amoghavarsha I ascended to the throne in 815 A.D. at the age of 14 after the death of his father Govind III. He was an accomplished poet and scholar. He wrote the Kavirajamarga, the earliest extant literary work in Kannada.', NULL, '2023-01-06 14:52:23', '2023-01-06 14:52:23'),
(40, 39, 1, 'Paramesvaravarman II was succeeded by 12-year-old Nandivarman II who belonged to the collateral line of Pallavas called the Kadavas. Nandivarman II has a long reign as he ruled from 731 A.D. to 795 A.D.', NULL, '2023-01-06 14:52:23', '2023-01-06 14:52:23'),
(41, 40, 1, 'The Later Chalukyan King Somesvara I came to the throne in 1042 AD. He gave up Manyakheta as his capital and established Kalyanpura of Kalyana as his new capital. His struggle with the Chola empire continued.', NULL, '2023-01-06 14:52:23', '2023-01-06 14:52:23'),
(42, 41, 1, 'Rajadhiraja who ruled the Chola kingdom from 1044 A.D. to 1052 A.D. was the son and the successor of Emperor Rajendra Chola I. He was a great warrior who always led from the front standing shoulder to shoulder with his men on front lines.', NULL, '2023-01-06 14:52:23', '2023-01-06 14:52:23'),
(43, 42, 1, 'Maravarman Rajasimha I was a Pandya king of early medieval South India. He was the son and successor of Koccadiyan Ranadhira and ruled the Pandya empire from 730 A.D. to 765 A.D. He is remembered for his important successes against the Pallavas and in the Kongu country.', NULL, '2023-01-06 14:52:23', '2023-01-06 14:52:23'),
(44, 43, 1, 'The third Sangam was held at the present Madurai in Tamil Nadu. The third sangam had 449 poets and the important ones among them include Nakkirar, Iraiyanar, Kapilar, Paranar, Sattanar, Auvikyar etc.', NULL, '2023-01-06 14:52:23', '2023-01-06 14:52:23'),
(45, 44, 1, 'Nedumthokai, Kurumthokai, Nattinai, Ainkurunnu, Pathittupattu, Paripadla, Kuttu and Vari are the works of third sangam. It lasted for 1850 years and was patronised by 49 kings.', NULL, '2023-01-06 14:52:24', '2023-01-06 14:52:24'),
(46, 45, 1, 'Major items of export to the Romans during the Sangam period were the Indian products such as spices, perfumes, jewels, ivory and fine textiles. Several precious and semi-precious stones  were also exported.', NULL, '2023-01-06 14:52:24', '2023-01-06 14:52:24'),
(47, 46, 1, 'Surkotada site contains horse remains dated to 2000 BC, which is considered a significant observation with respect to Indus Valley Civilization. During 1974, Archaeological Survey of India undertook excavation at this site and J.P. Joshi and A.K. Sharma reported findings of horse bones at all levels.', NULL, '2023-01-06 14:52:24', '2023-01-06 14:52:24'),
(48, 47, 1, 'In South India, the Bhakti movement originated in Tamil Nadu sometime after the 7th century AD with the Alwars (Vaishnava saints) and the Adiyars (Shaiva saints). With their songs of longing,they worshipped Hindu god of Vishnu. The collection of their hymns are known as Divya Prabandha. All the saints were male except one named Andal.', NULL, '2023-01-06 14:52:24', '2023-01-06 14:52:24'),
(49, 48, 1, 'There are four Vedas: the Rig-Veda, the Yajur Veda, the Sama Veda and the Atharva Veda. On the other hand, Vishnu Purana is a religious Hindu text and one of the eighteen Mahapuranas. It has been given the name Puranaratna.', NULL, '2023-01-06 14:52:24', '2023-01-06 14:52:24'),
(50, 49, 1, 'SIDBI has joined hands with Bihar Government with an aim to promote and help the MSME sector of the state. In this regard, it has signed MoUs with the Industries Department of the state and Bihar Industrial Area Development Authority (BIADA).', NULL, '2023-01-06 14:52:24', '2023-01-06 14:52:24'),
(51, 50, 12, NULL, NULL, '2023-01-26 12:55:27', '2023-01-26 12:55:27'),
(52, 51, 12, NULL, NULL, '2023-01-26 13:03:14', '2023-01-26 13:03:14'),
(53, 52, 12, NULL, NULL, '2023-01-26 13:13:32', '2023-01-26 13:13:32'),
(54, 53, 12, NULL, NULL, '2023-01-26 13:15:23', '2023-01-26 13:15:23'),
(55, 54, 12, NULL, NULL, '2023-01-26 13:15:40', '2023-01-26 13:15:40'),
(56, 55, 12, NULL, NULL, '2023-01-26 13:19:35', '2023-01-26 13:19:35'),
(57, 56, 12, NULL, NULL, '2023-01-26 13:28:38', '2023-01-26 13:28:38'),
(58, 57, 12, NULL, NULL, '2023-01-26 13:29:00', '2023-01-26 13:29:00'),
(59, 58, 12, NULL, NULL, '2023-01-26 13:53:59', '2023-01-26 13:53:59'),
(60, 59, 13, NULL, NULL, '2023-01-28 02:35:32', '2023-01-28 02:35:32'),
(61, 60, 14, NULL, NULL, '2023-01-28 22:50:40', '2023-01-28 22:50:40');

-- --------------------------------------------------------

--
-- Table structure for table `quiz_start_details`
--

CREATE TABLE `quiz_start_details` (
  `id` bigint UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `chat_in_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'telegram id of quiz start in ',
  `started_by` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'telegram id of quiz start owner',
  `time` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('1','2','3','4','5') COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '1=>pending,2=>inprogress,3=>terminated,4=>complete,5=>retry',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `quiz_start_details_questions`
--

CREATE TABLE `quiz_start_details_questions` (
  `id` bigint UNSIGNED NOT NULL,
  `quiz_start_detail_id` bigint UNSIGNED NOT NULL,
  `question_id` bigint UNSIGNED DEFAULT NULL,
  `poll_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_closed` tinyint(1) DEFAULT NULL,
  `total_voter_count` bigint UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `scrap_table`
--

CREATE TABLE `scrap_table` (
  `id` int NOT NULL,
  `title` text NOT NULL,
  `question` text NOT NULL,
  `answer` text NOT NULL,
  `type` varchar(255) NOT NULL,
  `options_value` text NOT NULL,
  `hints` text NOT NULL,
  `publish` tinyint(1) NOT NULL DEFAULT '0',
  `crated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `question_id` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `scrap_table`
--

INSERT INTO `scrap_table` (`id`, `title`, `question`, `answer`, `type`, `options_value`, `hints`, `publish`, `crated_at`, `question_id`) VALUES
(1, 'Ancient Indian History Quiz Multiple Choice Questions (MCQs)', 'Which of the following term is used for a “school” of learning and teaching the branches of Vedas?', 'B', '1', 'Shakha{{,}}Charna{{,}}Ratha{{,}}Yajna', 'Charana refers to the Guru-pupil lineage or school for teaching and learning of Vedas in ancient India.', 0, '2023-01-06 18:39:14', '1'),
(2, 'Ancient Indian History Quiz Multiple Choice Questions (MCQs)', 'Which among conclusions has been derived from the debasement of the coins and gradual disappearance of gold coins during the post-Gupta period?', 'D', '1', 'Commodities became cheap{{,}}Gold Mining was stalled{{,}}Money economy was gradually replaced by Barter Economy{{,}}There was a decline in trade', 'Debasement of the coins and gradual disappearance of gold coins during the post-Gupta period indicates the Decline of Trade.', 0, '2023-01-06 18:39:26', '2'),
(3, 'Ancient Indian History Quiz Multiple Choice Questions (MCQs)', 'Which among the following plays was / were written by Harsha?', 'D', '1', 'Nagananda{{,}}Ratnavali{{,}}Priyadarshika{{,}}All of these', 'Harsha wrote three sanskrit plays- Nagananda, Ratnavali and Priyadarshika.', 0, '2023-01-06 18:39:37', '3'),
(4, 'Ancient Indian History Quiz Multiple Choice Questions (MCQs)', 'Who was the author of Aihole Inscription ?', 'A', '1', 'Ravikirti{{,}}Hiuen Tsang{{,}}Bharavi{{,}}Dandin', 'Aihole Inscription is a eulogy written by Ravikirti who was the court poet of Chalukya King Pulakesin II.', 0, '2023-01-06 18:39:48', '4'),
(5, 'Ancient Indian History Quiz Multiple Choice Questions (MCQs)', 'Who was the founder of Nanda dynasty?', 'A', '1', 'Mahapadmananda{{,}}Shisunaga{{,}}Dhanananda{{,}}Nandivardhan', 'Mahapadmananda (345 BC – 329 BC) was the founder of Nanda dynasty. Mahapadmananda was also known as Ekarat and Sarvakshatrantaka.', 0, '2023-01-06 18:40:01', '5'),
(6, 'Ancient Indian History Quiz Multiple Choice Questions (MCQs)', 'Who granted permission to Buddhist king of Ceylon Meghavarman to build a monastic at Bodh Gaya?', 'A', '1', 'Samudragupta{{,}}Kumargupta{{,}}Skandgupta{{,}}Chandragupta II', 'The Gupta emperor, Samudragupta granted permission to Buddhist king of Ceylon Meghavarman to build a monastry at Bodh Gaya. Hence, he was also known as Anukampavan (full of compassion).', 0, '2023-01-06 18:40:14', '6'),
(7, 'Ancient Indian History Quiz Multiple Choice Questions (MCQs)', 'Rummindei Pillar Inscription which talks about taxation in Maurya era has been found at which of the following places?', 'D', '1', 'Junagarh in Gujarat{{,}}Ranchi in Jharkhand{{,}}Bhabru in Rajasthan{{,}}Lumbini in Nepal', 'Lumbini Pillar Edict in Nepal is known as the Rummindei Pillar Inscription .The Lumbini Pillar Edict recorded that sometime after the twentieth year of his reign, Ashoka travelled to the Buddha’s birthplace and personally made offerings. He then had a stone pillar set up and reduced the taxes of the people in that area.', 0, '2023-01-06 18:40:26', '7'),
(8, 'Ancient Indian History Quiz Multiple Choice Questions (MCQs)', 'Which among the following Buddhist Canon is related to dealing with rules for monks and nuns ?', 'A', '1', 'Vinaya Pitaka{{,}}Sutta Pitaka{{,}}Abhidhamma Pitaka{{,}}None of the above', 'Tripitaka or Three Baskets is a traditional term used for various Buddhist scriptures. It is known as pali Canon in English. The three pitakas are Sutta Pitaka, Vinaya Pitaka and Abhidhamma Pitaka. Sutta Pitaka: It contains over 10 thousand suttas or sutras related to Buddha and his close companions. This also deals with the first Buddhist council which was held shortly after Buddha’s death, dated by the majority of recent scholars around 400 BC, under the patronage of king Ajatasatru with the monk Mahakasyapa presiding, at Rajgir. It is divided into various sections as shown in following graphics: Vinaya Pitaka: The subject matter of Vinay Pitaka is the monastic rules for monks and nuns. It can also be called as Book of Discipline. Its three books are Suttavibhanga, Khandaka and Parivara. Abhidhammapitaka deals with the philosophy and doctrine of Buddhism appearing in the suttas. However, it does not contain the systematic philosophical treatises. There are 7 works of Abhidhamma Pitaka which most scholars agree that don’t represent the words of Buddha himself.', 0, '2023-01-06 18:40:39', '8'),
(9, 'Ancient Indian History Quiz Multiple Choice Questions (MCQs)', 'The following were the immediate successors of imperial Mauryas?', 'D', '1', 'Pandyas{{,}}Kushans{{,}}Nandas{{,}}Sungas', 'The Shunga Empire was an ancient Indian dynasty from Magadha that controlled areas of the central and eastern Indian subcontinent from around 185 to 75 BCE. They are the immediate successors of imperial Mauryas.', 0, '2023-01-06 18:40:55', '9'),
(10, 'Ancient Indian History Quiz Multiple Choice Questions (MCQs)', 'Giak & Kiari are located in which of the following ?', 'A', '1', 'Ladakh{{,}}Orissa{{,}}Kuchh{{,}}Assam', 'Giak & Kiari are located in Ladakh. They are examples of Neolithic sites', 0, '2023-01-06 18:41:11', '10'),
(11, 'Ancient Indian History Quiz Multiple Choice Questions (MCQs)', 'The harappans used intaglio seals, made mostly of carved and fired steatite :', 'D', '1', 'As an export commodity.{{,}}As a medium of exchange in place of coins.{{,}}As amulets and charms to ward off evil spirits.{{,}}For marking their goods and property.', 'The seals of the ancient Harappan’s were probably used in much the same way they are today, to sign letters or for commercial transactions. The Harappan seals were used for marking goods and bales of merchandise. (Hence Option d. is correct)', 0, '2023-01-06 18:43:37', '11'),
(12, 'Ancient Indian History Quiz Multiple Choice Questions (MCQs)', 'Consider the following differences between North and South temple architecture:', 'C', '1', '1 Only{{,}}2 Only{{,}}Both 1 & 2{{,}}Neither 1 nor 2', 'Both are correct statements.  The North Indian temples follow the Ngara style of architecture while the south Indian temples follow the Dravidian architecture. Shikhara is in Nagara style and Vimana is Davidian style.', 0, '2023-01-06 18:43:49', '12'),
(13, 'Ancient Indian History Quiz Multiple Choice Questions (MCQs)', 'Which of the following physicians was a contemporary of Gautama Buddha ?', 'C', '1', 'Charaka{{,}}Sushena{{,}}Jivaka{{,}}Vagbhatta', 'Jivaka was the Buddha’s personal physician and is considered as the father of Buddhist medicine.', 0, '2023-01-06 18:44:00', '13'),
(14, 'Ancient Indian History Quiz Multiple Choice Questions (MCQs)', 'With reference to the Aranyakas, which among the following statements is / are correct?', 'B', '1', 'Only 1 & 2{{,}}Only 1 & 3{{,}}1, 2 & 3{{,}}Only 3', 'The Aranyakas were written in Forests and are concluding parts of the Brahmans. Aranyakas don’t lay much emphasis on rites, ritual and sacrifices but have philosophy and mysticism. So they have moral science and philosophy. It also provides the details of the rishis who lived in jungles. The end portions of many Brahmanas have an esoteric content, called the ‘Aranyakas’', 0, '2023-01-06 18:44:12', '14'),
(15, 'Ancient Indian History Quiz Multiple Choice Questions (MCQs)', 'Which of the following find their origin in the Gupta Era?', 'A', '1', 'Only 1{{,}}Only 1 & 3{{,}}1, 2 & 3{{,}}None', 'Mahayana Buddhism, also known as the Great Vehicle, is the form of Buddhism prominent in North Asia, including China, Mongolia, Tibet, Korea, and Japan. It started in the first century C.E. and Vedanta school is of much earlier time period.', 0, '2023-01-06 18:44:24', '15'),
(16, 'Ancient Indian History Quiz Multiple Choice Questions (MCQs)', 'Which among the following is / are features of the Post-Gupta society of India?', 'A', '1', 'Only 1, 2 & 3{{,}}Only 1, 3 & 4{{,}}Only 2, 3 & 4{{,}}1, 2, 3 & 4', 'The summary of the societal conditions in early medieval India are as follows: Political decentralization The new polity is characterized by decentralization and hierarchy, features suggested by the presence of a wide range of semi-autonomous rulers, Samantas, Mahasamantas and others and the hierarchized positioning of numerous Rajapurushas employed by royal courts. Emergence of Landed intermediaries This is the hallmark of Indian feudal social formation and is seen to be linked both to the disintegration and decentralization of state authority and to major changes in the structure of agrarian relations. The emergence of landed intermediaries- a dominant landholding social group absent in the early historical period- is linked to the practice of land grants which began with the Satavahanas. The earliest land grants belonging to the first century BC were given to the Buddhist priests and Brahmans and other religious establishments. However, in the post- gupta period even administrative officials were granted land. The landed beneficiaries were given both powers of taxation and coercion, leading to the disintegration of the central authority. The secular recipients of the grants and the autonomous holders of land are generally termed as fief holders and free holders. Localization of economy There was a economy to self-sufficient villages as units of production. Thus, ruralisation was an important dimension of the transition process. This change was the result of the decline of early historical urban centres and commercial networks leading to the practice of payment in land grants instead of earlier practice of remuneration in cash, migration of different urban social groups to rural areas, expansion of agrarian space and the crystallization of Jajmani type of relationships in the rural areas. According to one formulation, fief holders and free-holders in rural society emerged as agents of social change in the later phase of early medieval society, generating once again such features of early historical economy as trade, urbanization and a market economy. Subjection of the peasantry Likened sometimes to serfdom, characterizing the of the subjection of peasantry, such as immobility, forced labour (vishti) and the payment of revenue at exorbitantly high rates- all point to the nature of stratification in Post-Gupta society. The condition of the peasantry in this pattern of rural stratification was in sharp contrast to what the agrarian structure in early historical India represented, since that structure was dominated by free vaishya peasants and labour services provided by the Shudras. It is important to note here that although the earliest example of sharecroppers being transferred along with the land can be traced in the third century pallava inscription from Andhra, Orissa and Deccan, from the sixth century AD onwards sharecroppers and peasants were particularly asked to stick to the land granted to the beneficiaries. The custom became fairly common in the post-gupta period and the villages transferred to the grantees are known as dhana-jana-sahita, janata-samriddha and saprativasi-jana- sameta. Thus the artisans and peasants were asked not to leave the village granted to the beneficiaries or migrate to tax-free village. Proliferation of castes A striking social development from about the seventh century onwards was the proliferation of castes. The Brahmavaivarta Purana, a seventh century work, counts 100 castes including 61 castes noted by Manu, but the Vishnudharmottara Purana (8th century) states that thousands of mixed castes are produced by the connection of vaishyas women with men of lower castes. In fact, proliferation affected the brahmanas, the Rajputs, and above all, the shudras and untouchables. Increasing pride of birth, characteristic of feudal society, and the accompanying self-sufficient village economy, which prevented both spatial and occupational mobility, gave rise to many castes. The guilds of artisans gradually hardened into castes due to lack of mobility in post-gupta times. The absorption of the tribal peoples into the brahmanical fold, though as old as vedic times, was mainly based on conquests. Coupled with the process of large-scale religious land grants. Acculturation assumed enormous dimensions and considerably added to the variety of the shudras and so-called mixed castes. According to Prof. R.S. Sharma social changes were mainly the product of certain economic developments, such as land grants and large scale transfers of land revenues and land to both secular and religious elements, decline of trade and commerce, loss of mobility of artisans, peasants and traders, unequal distribution of land an power etc. He holds the economic factor responsible for the emergence of certain new castes and decline of certain old ones. Thus the constant transfer of land of land revenues made by princes to priests, temples and officials led to the rise and growth of the scribe or the Kayastha community which undermined the monopoly of Brahmans as writers and scribes. Similarly, the decline of trade and commerce led to the decline in the position of the Vaishyas. The process of proliferation and multiplication of castes was yet another marked feature of the social life of the period.', 0, '2023-01-06 18:44:35', '16'),
(17, 'Ancient Indian History Quiz Multiple Choice Questions (MCQs)', 'Consider the following:', 'D', '1', '1 & 2 Only{{,}}3 Only{{,}}2 & 3 Only{{,}}1, 2 & 3', 'The important sites of Ahar Culture were Aahar (Rajasthan), Balathal, Gilund etc. Kayatha Culture was mainly located near Chambal and its tributaries. Malwa Culture was distributed on the banks of Narmada & its tributaries in Gujarat.', 0, '2023-01-06 18:44:47', '17'),
(18, 'Ancient Indian History Quiz Multiple Choice Questions (MCQs)', 'Consider the following pairs of Harappan sites with their burial customs:', 'D', '1', '1 & 2 Only{{,}}3 Only{{,}}2 & 3 Only{{,}}1, 2 & 3', 'Important burial sites of Indus Valley Civilization are Harappa, Kalibangan, Rakhigarhi, Lothal, Rojdi, and Ropar. Lothal is one of the most prominent cities of the ancient Indus valley civilization. Evidence of double burial (burying a male and a female in a single grave) has been found here. The most common method of burial was found in Kalibangan. It was to place the body of the deceased in an extended position, with the head towards the north, in a simple pit or brick chamber. Evidence of pot-burial has been found in Surkotada.', 0, '2023-01-06 18:44:58', '18'),
(19, 'Ancient Indian History Quiz Multiple Choice Questions (MCQs)', 'The terms Swarajya and Vairajya were used in Later Vedic Period to denote which form of government?', 'D', '1', 'Democracy{{,}}Socialist{{,}}Autocratic{{,}}Republic', 'The terms \"swarajya\" and \"vairajya\" were used for republic government. The place where only a particular person used to rule mainly was called \"Rajya\" (state).', 0, '2023-01-06 18:45:09', '19'),
(20, 'Ancient Indian History Quiz Multiple Choice Questions (MCQs)', 'Consider the following:', 'A', '1', '1, 2 & 3 Only{{,}}2, 3 & 4 Only{{,}}1, 3 & 4 Only{{,}}1, 2, 3 & 4', 'Tathagat is a Pali and Sanskrit word; Gautama Buddha uses it when referring to himself. Tathagat is one who attained the truth. Syadvada is associated to Jainism. It states that all judgments are conditional, holding good only in certain conditions & circumstances. Upasaka (masculine) and Upasikas (feminine) were the titles given to followers of the Buddha who undertook certain vows, but were not monks, nuns, or monastics. Gautama Buddha left his family to become a ascetic in search of truth. This is called “the great departure\' or Mahabhinishkramana.', 0, '2023-01-06 18:45:20', '20'),
(21, 'Ancient Indian History Quiz Multiple Choice Questions (MCQs)', 'In which among the following Mahajanapada, Lord Buddha attained parinirvana?', 'C', '1', 'Anga{{,}}Magadha{{,}}Malla{{,}}Vatsa', 'Buddha attained Niravana at Kushinagara, a village some 180 Kilometers from Varanasi and it was in the state of Mallas.', 0, '2023-01-06 18:45:33', '21'),
(22, 'Ancient Indian History Quiz Multiple Choice Questions (MCQs)', 'Which of the following site is known for cave paintings of Mesolithic Age?', 'C', '1', 'Dholavira{{,}}Ellora{{,}}Bhimbetka{{,}}Soan Valley', 'The period between 12,000 years ago till about 10,000 years ago is called Mesolithic. In Greek ‘meso’ means the middle and ‘lithic’ means stone. Hence, the Mesolithic stage of prehistory is also known as the Middle Stone Age. It was the transitional phase between the Palaeolithic and the Neolithic Ages Bhimbetka caves which are located in Madhya Pradesh is known for Excellent cave paintings of Mesolithic Age', 0, '2023-01-06 18:45:45', '22'),
(23, 'Ancient Indian History Quiz Multiple Choice Questions (MCQs)', 'What was the name given to the head of the family during the Rig Vedic Age?', 'C', '1', 'Rajan{{,}}Jana{{,}}Kulapa{{,}}Vis', 'All the social units during the Rig Vedic Age were based on brotherhood. The Kula or the Family was the basic social unit. The head of the family was known as Kulapa. The Rig Vedic Society followed patrilineal system.', 0, '2023-01-06 18:45:56', '23'),
(24, 'Ancient Indian History Quiz Multiple Choice Questions (MCQs)', 'Which of the following are correct regarding the Rig Vedic age?', 'D', '1', 'Only 1{{,}}Only 1 & 2{{,}}Only 2 & 3{{,}}1, 2 & 3', 'The Rig Vedic Aryans used horses, chariots, better arms made of bronze. They also introduced spoked wheel. They were acquainted with sowing, harvesting and threshing.', 0, '2023-01-06 18:46:07', '24'),
(25, 'Ancient Indian History Quiz Multiple Choice Questions (MCQs)', 'Which of the following doctrine is added by Jainism?', 'D', '1', 'Ahimsa{{,}}Satya{{,}}Asteya{{,}}Brahmacharya', 'Th fifth doctrine of Jainism- celibacy/chastity (brahmacharya) was added by Mahavira. Brahmacharya means total abstinence from sensual pleasure. It is a form of infatuating force, which sets aside all virtues and reason at the time of indulgence.', 0, '2023-01-06 18:46:19', '25'),
(26, 'Ancient Indian History Quiz Multiple Choice Questions (MCQs)', 'The law of dependent organization or patichcha-samuppada refers to', 'B', '1', 'Path to attain moksha.{{,}}Explains the reasons of dukkha as well as the key to its liberation.{{,}}Gives the account of pancha-khanda{{,}}Circle of life and death.', 'The law of dependent organization or patichcha-samuppada as mentioned in Buddhism explains the reason of all dukkha, as well as the key to its liberation. There are 12 components arranged in a wheel and one leading to next.', 0, '2023-01-06 18:46:30', '26'),
(27, 'Ancient Indian History Quiz Multiple Choice Questions (MCQs)', 'Who was appointed as the viceroy of Taxila and Ujjain during the reign of Bindusara?', 'C', '1', 'Susima{{,}}Kunala{{,}}Ashoka{{,}}Vaisya', 'Ashoka was appointed the viceroy of both Taxila and Ujjain during the reign of Mauryan King Bindusara. Both the cities handled commercial activities. Asoka had himself formally crowned in 268 BC.', 0, '2023-01-06 18:46:41', '27'),
(28, 'Ancient Indian History Quiz Multiple Choice Questions (MCQs)', 'Which of the following were the officers responsible for the suppression of criminals?', 'B', '1', 'Dharmasthas{{,}}Pradeshtris{{,}}Mahamattas{{,}}None of the above', 'According to Ashokan inscriptions Pradeshtris were the officers responsible for the suppression of criminals in the Mauryan empire. During that period various civil as well as criminal courts functioned at the local level right from village to province.', 0, '2023-01-06 18:46:53', '28'),
(29, 'Ancient Indian History Quiz Multiple Choice Questions (MCQs)', 'Which of the following period saw the emergence of rock-cut architecture?', 'B', '1', 'Gupta period{{,}}Maurya period{{,}}Vedic period{{,}}Post-vedic period', 'Stone culture dramatically emerged as the principal medium of Indian arts in the Mauryan period. Many art forms such as stone sculptures, ring stones, disc stones, terracotta figurines, and stupa architecture were patronized by different Mauryan kings. The Maurya period also saw the beginning of rock-cut architecture.', 0, '2023-01-06 18:47:04', '29'),
(30, 'Ancient Indian History Quiz Multiple Choice Questions (MCQs)', 'Pushyamitra Shunga died in which of the following years?', 'A', '1', '151 BCE{{,}}165 BCE{{,}}180 BCE{{,}}100 AD', 'According to Puranas, Pushyamitra Shunga who founded the Shunga dynasty in 185 or 186 BCE, died in the year 151 BCE. His reign is believed to have lasted for around 36 years.', 0, '2023-01-06 18:47:15', '30'),
(31, 'Ancient Indian History Quiz Multiple Choice Questions (MCQs)', 'Which of the following refers to the royal share of merchandise brought into a town or harbour by merchants?', 'A', '1', 'Shulka{{,}}Bhaga{{,}}Bhoga{{,}}Kara', 'Shulka during the Gupta period was a royal share of merchandise which was brought into a town or harbour by merchants. Therefore it can also be equated with the customs and tolls.', 0, '2023-01-06 18:47:28', '31'),
(32, 'Ancient Indian History Quiz Multiple Choice Questions (MCQs)', 'Which of the following Vakataka king took the title of Samrat?', 'A', '1', 'Pravarasena{{,}}Vindhyashakti{{,}}Prithvisena{{,}}Rudrasena', 'Pravarasena was the second ruler of the Vakataka Dynasty. He performed an asvamedha sacrifice and a vajapeya sacrifice. He was the only Vakataka king who took the title of Samrat.', 0, '2023-01-06 18:47:39', '32'),
(33, 'Ancient Indian History Quiz Multiple Choice Questions (MCQs)', 'Which of the following was the court poet of Harsha?', 'B', '1', 'Kalidasa{{,}}Banabhatta{{,}}Harishena{{,}}Bhasa', 'Harshacharita was wriiten by Banabhatta. It is the biography of Harsha. Bana was a Brahman. He was the court poet of Harsha. Harshacharita conatins the history of the reign of Harsha.', 0, '2023-01-06 18:47:50', '33'),
(34, 'Ancient Indian History Quiz Multiple Choice Questions (MCQs)', 'Which of the following  actually laid the foundations of the Pushyabhuti dynasty?', 'A', '1', 'Prabhakar Vardhana{{,}}Harsha Vardhana{{,}}Rajya Vardhana{{,}}Grahavarman', 'Prabhakar Vardhana who ruled around mid-6th century CE was the fourth king of the Pushyabhuti dynasty. He actually laid the foundations of the Pushyabhuti dynasty. Prabhakar Vardhana was considered a great general with many military victories.', 0, '2023-01-06 18:48:02', '34'),
(35, 'Ancient Indian History Quiz Multiple Choice Questions (MCQs)', 'Which of the following Chalukya king fought with the Yasovarman?', 'A', '1', 'Vijayaditya{{,}}Vakpatiraja{{,}}Manorathavarman{{,}}Vinayaditya', 'According to some inscriptions, during the reign of Chalukya king Vinayaditya who ruled from 681 A.D. to 696 A.D. of Badami, his son Vijayaditya fought with the “lord of the whole of northern regions” Yasovarman.', 0, '2023-01-06 18:48:13', '35'),
(36, 'Ancient Indian History Quiz Multiple Choice Questions (MCQs)', 'Which of the following kings was identified as king Juzr from the travel accounts of the 9th-century Arab merchant, Sulaiman?', 'D', '1', 'Vatsaraja{{,}}Nagabhatta I{{,}}Harichandra{{,}}Mihir Bhoja', 'Mihir Bhoja is also identified as the king Juzr from the travel accounts of the 9th-century Arab merchant, Sulaiman. According to the Arab merchant Sulaiman, Mihir Bhoja possessed great military power and riches.', 0, '2023-01-06 18:48:24', '36'),
(37, 'Ancient Indian History Quiz Multiple Choice Questions (MCQs)', 'Who was the author of Ajita-Santi-Stava?', 'B', '1', 'Dharmadasagani{{,}}Nandishena{{,}}Mahesvarasuri{{,}}Uddyotanasuri', 'Nandishena was the author of Ajita-Santi-Stava. Kuvalayamala was written by Uddyotanasuri. Dharmadasagani wrote Updesmala and  Mahesvarasuri composed a religious tale called Jnanapanchami-Katha.', 0, '2023-01-06 18:48:35', '37'),
(38, 'Ancient Indian History Quiz Multiple Choice Questions (MCQs)', 'Rashtrakutas are believed to the feduatories of which of the following?', 'A', '1', 'Chalukyas{{,}}Chahmanas{{,}}Chandellas{{,}}Vakatakas', 'The Rashtrakutas are presumed to be a feudatory of the Chalukyas. They rose to power in Deccan between c.753 and 975 CE. Rashtrakuta means the chief of a rashtra.', 0, '2023-01-06 18:48:47', '38'),
(39, 'Ancient Indian History Quiz Multiple Choice Questions (MCQs)', 'Which of the following kings wrote Kavirajamarga?', 'C', '1', 'Dhruva{{,}}Govind II{{,}}Amoghvarsha I{{,}}Dantidurga', 'Amoghavarsha I ascended to the throne in 815 A.D. at the age of 14 after the death of his father Govind III. He was an accomplished poet and scholar. He wrote the Kavirajamarga, the earliest extant literary work in Kannada.', 0, '2023-01-06 18:48:58', '39'),
(40, 'Ancient Indian History Quiz Multiple Choice Questions (MCQs)', 'Paramesvaravarman II was succeeded by which of the following kings?', 'D', '1', 'Virakurcha{{,}}Dantivarman{{,}}Nandivarman I{{,}}Nandivarman II', 'Paramesvaravarman II was succeeded by 12-year-old Nandivarman II who belonged to the collateral line of Pallavas called the Kadavas. Nandivarman II has a long reign as he ruled from 731 A.D. to 795 A.D.', 0, '2023-01-06 18:49:09', '40'),
(41, 'Ancient Indian History Quiz Multiple Choice Questions (MCQs)', 'Somesvara I established his capital at which of the following places?', 'B', '1', 'Manyakheta{{,}}Kalyanpura{{,}}Badami{{,}}Kanchi', 'The Later Chalukyan King Somesvara I came to the throne in 1042 AD. He gave up Manyakheta as his capital and established Kalyanpura of Kalyana as his new capital. His struggle with the Chola empire continued.', 0, '2023-01-06 18:49:21', '41'),
(42, 'Ancient Indian History Quiz Multiple Choice Questions (MCQs)', 'Which of the following kings succeeded Rajendra I?', 'C', '1', 'Virarajendra{{,}}Rajendra II{{,}}Rajadhiraja{{,}}Athirajendra', 'Rajadhiraja who ruled the Chola kingdom from 1044 A.D. to 1052 A.D. was the son and the successor of Emperor Rajendra Chola I. He was a great warrior who always led from the front standing shoulder to shoulder with his men on front lines.', 0, '2023-01-06 18:49:33', '42'),
(43, 'Ancient Indian History Quiz Multiple Choice Questions (MCQs)', 'Which of the following kings succeeded the king Koccadiyan Ranadhira?', 'B', '1', 'Arikesary Maravarman{{,}}Maravarman Rajasimha I{{,}}Varguna I{{,}}Nedum-Cheliyan', 'Maravarman Rajasimha I was a Pandya king of early medieval South India. He was the son and successor of Koccadiyan Ranadhira and ruled the Pandya empire from 730 A.D. to 765 A.D. He is remembered for his important successes against the Pallavas and in the Kongu country.', 0, '2023-01-06 18:49:44', '43'),
(44, 'Ancient Indian History Quiz Multiple Choice Questions (MCQs)', 'Where was the third Sangam held?', 'A', '1', 'Madurai{{,}}Thenmadurai{{,}}Kabadapuram{{,}}Vellore', 'The third Sangam was held at the present Madurai in Tamil Nadu. The third sangam had 449 poets and the important ones among them include Nakkirar, Iraiyanar, Kapilar, Paranar, Sattanar, Auvikyar etc.', 0, '2023-01-06 18:49:56', '44'),
(45, 'Ancient Indian History Quiz Multiple Choice Questions (MCQs)', 'Nedumthokai, Kurumthokai, Nattinai, Ainkurunnu, Pathittupattu, Paripadla, Kuttu and Vari are the works of which of the following sangams?', 'C', '1', 'First sangam{{,}}Second sangam{{,}}Third sangam{{,}}None of the above', 'Nedumthokai, Kurumthokai, Nattinai, Ainkurunnu, Pathittupattu, Paripadla, Kuttu and Vari are the works of third sangam. It lasted for 1850 years and was patronised by 49 kings.', 0, '2023-01-06 18:50:07', '45'),
(46, 'Ancient Indian History Quiz Multiple Choice Questions (MCQs)', 'Which of the following were the major items of export to the Romans during the Sangam period?', 'D', '1', 'Only 1 & 2{{,}}Only 2 & 3{{,}}Only 2, 3 & 4{{,}}1, 2, 3 & 4', 'Major items of export to the Romans during the Sangam period were the Indian products such as spices, perfumes, jewels, ivory and fine textiles. Several precious and semi-precious stones  were also exported.', 0, '2023-01-06 18:50:18', '46'),
(47, 'Ancient Indian History Quiz Multiple Choice Questions (MCQs)', 'Remains of horse have been found from which site of Indus Valley Civilization?', 'C', '1', 'Mohenjo-Daro{{,}}Lothal{{,}}Surkotada{{,}}Suktagen Dor', 'Surkotada site contains horse remains dated to 2000 BC, which is considered a significant observation with respect to Indus Valley Civilization. During 1974, Archaeological Survey of India undertook excavation at this site and J.P. Joshi and A.K. Sharma reported findings of horse bones at all levels.', 0, '2023-01-06 18:50:29', '47'),
(48, 'Ancient Indian History Quiz Multiple Choice Questions (MCQs)', 'Which among these modern States did the Alwar saints originate from?', 'B', '1', 'Maharashtra{{,}}Tamil Nadu{{,}}Kerala{{,}}Karnataka', 'In South India, the Bhakti movement originated in Tamil Nadu sometime after the 7th century AD with the Alwars (Vaishnava saints) and the Adiyars (Shaiva saints). With their songs of longing,they worshipped Hindu god of Vishnu. The collection of their hymns are known as Divya Prabandha. All the saints were male except one named Andal.', 0, '2023-01-06 18:50:41', '48'),
(49, 'Ancient Indian History Quiz Multiple Choice Questions (MCQs)', 'Which of these is not a part of Veda?', 'B', '1', 'Rigveda{{,}}Vishnu Purana{{,}}Samveda{{,}}Yajurveda', 'There are four Vedas: the Rig-Veda, the Yajur Veda, the Sama Veda and the Atharva Veda. On the other hand, Vishnu Purana is a religious Hindu text and one of the eighteen Mahapuranas. It has been given the name Puranaratna.', 0, '2023-01-06 18:50:52', '49'),
(50, 'Ancient Indian History Quiz Multiple Choice Questions (MCQs)', 'With the goal of promoting and assisting the MSME sector, SIDBI has partnered with which state government?', 'D', '1', 'Jharkhand{{,}}Andhra Pradesh{{,}}Arunachal Pradesh{{,}}Bihar', 'SIDBI has joined hands with Bihar Government with an aim to promote and help the MSME sector of the state. In this regard, it has signed MoUs with the Industries Department of the state and Bihar Industrial Area Development Authority (BIADA).', 0, '2023-01-06 18:51:03', '50');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `user_type` tinyint(1) DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `telegram_chat_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `user_type`, `password`, `remember_token`, `created_at`, `updated_at`, `telegram_chat_id`) VALUES
(1, 'Admin', 'admin@admin.com', '2022-07-23 05:03:00', 2, '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'dhTZqTMGPnmXSsfuMMAWj1NXFR1zcC9iiA7KT1NmetI3U1500ExaNzm6hR36', '2022-07-23 05:03:00', '2022-07-23 05:03:00', NULL),
(2, 'Norbert Nolan Jr.', 'lourdes.mccullough@example.org', '2022-07-23 05:03:00', NULL, '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'JmMWc0xetl', '2022-07-23 05:03:00', '2022-07-23 05:03:00', NULL),
(3, 'Bernadette Shields', 'tillman10@example.org', '2022-07-23 05:03:00', NULL, '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'W01d9cb0jF', '2022-07-23 05:03:00', '2022-07-23 05:03:00', NULL),
(4, 'Ludie Sipes', 'charlene76@example.org', '2022-07-23 05:03:00', NULL, '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'F2etVNO10j', '2022-07-23 05:03:00', '2022-07-23 05:03:00', NULL),
(5, 'Kendrick Kilback', 'isac.flatley@example.net', '2022-07-23 05:03:00', NULL, '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'issCY1cyLQ', '2022-07-23 05:03:00', '2022-07-23 05:03:00', NULL),
(6, 'Vicky Beier DVM', 'quigley.vivianne@example.com', '2022-07-23 05:03:00', NULL, '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'fLIRDPdeHg', '2022-07-23 05:03:00', '2022-07-23 05:03:00', NULL),
(7, 'Hayley Lang Jr.', 'ferry.general@example.net', '2022-07-23 05:03:00', NULL, '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '30sLi1x0yL', '2022-07-23 05:03:00', '2022-07-23 05:03:00', NULL),
(8, 'Christine Hettinger', 'brady44@example.org', '2022-07-23 05:03:00', NULL, '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'HN2wwESLO9', '2022-07-23 05:03:00', '2022-07-23 05:03:00', NULL),
(9, 'Lexi Boyle', 'lauretta54@example.com', '2022-07-23 05:03:00', NULL, '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'tBwRLqbX7g', '2022-07-23 05:03:00', '2022-07-23 05:03:00', NULL),
(10, 'Darren Stiedemann', 'violet76@example.com', '2022-07-23 05:03:00', NULL, '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '8SxyNhU8jL', '2022-07-23 05:03:00', '2022-07-23 05:03:00', NULL),
(11, 'Dr. Theresia Torphy', 'ivory.thompson@example.net', '2022-07-23 05:03:00', NULL, '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'fbNO2LGPXr', '2022-07-23 05:03:00', '2022-07-23 05:03:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `verify_bot_mails`
--

CREATE TABLE `verify_bot_mails` (
  `id` bigint UNSIGNED NOT NULL,
  `chat_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `answered_users`
--
ALTER TABLE `answered_users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bot_users`
--
ALTER TABLE `bot_users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `configurations`
--
ALTER TABLE `configurations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `errors`
--
ALTER TABLE `errors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `events`
--
ALTER TABLE `events`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `event_application_forms`
--
ALTER TABLE `event_application_forms`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `event_topics`
--
ALTER TABLE `event_topics`
  ADD PRIMARY KEY (`id`),
  ADD KEY `event_topics_event_id_foreign` (`event_id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `jobs`
--
ALTER TABLE `jobs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `jobs_queue_index` (`queue`);

--
-- Indexes for table `messages_with_bots`
--
ALTER TABLE `messages_with_bots`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `options`
--
ALTER TABLE `options`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `process_chats`
--
ALTER TABLE `process_chats`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `questions`
--
ALTER TABLE `questions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `quizzes`
--
ALTER TABLE `quizzes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `quiz_codes`
--
ALTER TABLE `quiz_codes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `quiz_questions`
--
ALTER TABLE `quiz_questions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `quiz_start_details`
--
ALTER TABLE `quiz_start_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `quiz_start_details_questions`
--
ALTER TABLE `quiz_start_details_questions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scrap_table`
--
ALTER TABLE `scrap_table`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `verify_bot_mails`
--
ALTER TABLE `verify_bot_mails`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `answered_users`
--
ALTER TABLE `answered_users`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `bot_users`
--
ALTER TABLE `bot_users`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `configurations`
--
ALTER TABLE `configurations`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `contacts`
--
ALTER TABLE `contacts`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `errors`
--
ALTER TABLE `errors`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `events`
--
ALTER TABLE `events`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `event_application_forms`
--
ALTER TABLE `event_application_forms`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `event_topics`
--
ALTER TABLE `event_topics`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jobs`
--
ALTER TABLE `jobs`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `messages_with_bots`
--
ALTER TABLE `messages_with_bots`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `options`
--
ALTER TABLE `options`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=223;

--
-- AUTO_INCREMENT for table `process_chats`
--
ALTER TABLE `process_chats`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `questions`
--
ALTER TABLE `questions`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=61;

--
-- AUTO_INCREMENT for table `quizzes`
--
ALTER TABLE `quizzes`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `quiz_codes`
--
ALTER TABLE `quiz_codes`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `quiz_questions`
--
ALTER TABLE `quiz_questions`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=62;

--
-- AUTO_INCREMENT for table `quiz_start_details`
--
ALTER TABLE `quiz_start_details`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `quiz_start_details_questions`
--
ALTER TABLE `quiz_start_details_questions`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `scrap_table`
--
ALTER TABLE `scrap_table`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `verify_bot_mails`
--
ALTER TABLE `verify_bot_mails`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `event_topics`
--
ALTER TABLE `event_topics`
  ADD CONSTRAINT `event_topics_event_id_foreign` FOREIGN KEY (`event_id`) REFERENCES `events` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
