@extends('admin.layout.main')
@section('title') Group @endsection
@php
    $group = isset($group)?$group:(new App\Models\Group());
    $isView = ($isView??false);
@endphp
@section('body')

<!-- Table Start -->
<div class="container-fluid pt-4 px-4">
	<div class="row g-4">
		<div class="col-sm-12 col-xl-12">
			<div class="bg-light rounded h-100 p-4">
				<h6 class="mb-4">Add Group</h6>
                <form method="POST" action="{{isset($group->id)?route('admin.group.update',$group->id):route('admin.group.store')}}">
                    @csrf()
                    @isset($group->id)
                        @method('PUT')
                    @endisset
                    <x-admin.input name="name" :isView="$isView" :value="$group->name"/>
                    <x-admin.input name="code" :isView="$isView" :value="$group->code"/>

                    <div class="form-group">
                        <label class="form-label">Status</label>
                        <select name="status" class="form-control" @if($isView) readonly @endif>
                            <option value="1" @selected($group->status == "1")>Active</option>
                            <option value="2"  @selected($group->status == "2")>Reject</option>
                        </select>
                    </div>
                    <div class="form-group mt-3">
                    @if(!$isView)
                    <button class="btn btn-primary" type="submit">Save </button>
                    @endif
                    <a href="{{route('admin.group')}}" class="btn btn-danger">Cancel</a>
                    </div>
                </form>
			</div>
		</div>
    </div>
</div>
@endsection
