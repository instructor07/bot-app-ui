@extends('admin.layout.main')
@section('title') Group @endsection
@section('body')
<!-- Table Start -->
<div class="container-fluid pt-4 px-4">
	<div class="row g-4">
		<div class="col-sm-12 col-xl-12">
            <a href="{{route('admin.group.create')}}" class="btn btn-primary">Add Group</a>
			<div class="bg-light rounded h-100 p-4">
				<h6 class="mb-4">Group Table</h6>
                @if(session('success'))
                    <div class="alert alert-success">
                        <p>{{session('success')}}</p>
                    </div>
                @endif
				<table class="table">
					<thead>
						<tr>
							<th scope="col">#</th>
							<th scope="col">Name</th>
							<th scope="col">Code</th>
							<th scope="col">Status</th>
                            <th scope="col">Action</th>
						</tr>
					</thead>
					<tbody>
						@foreach ($data as $index=>$val)
                        <tr>
                        <td scope="col">{{$index+1}}</td>
                        <td scope="col">{{$val->name}}</td>
                        <td scope="col">{{$val->code}}</td>
                        <td scope="col">{{$val->status}}</td>
                        <td scope="col">
                            <a href="{{route('admin.group.edit',$val->id)}}" class="btn btn-primary">Edit</a>
                            <a href="{{route('admin.group.show',$val->id)}}" class="btn btn-primary">View</a>
                            <form method="POST" action="{{route("admin.group.delete",$val->id)}}">
                                @csrf
                                @method('DELETE')
                            <button type="submit" class="btn btn-danger">Delete</button>
                            </form>
                        </td>
                        </tr>
                        @endforeach
					</tbody>
				</table>
			</div>
		</div>
    </div>
</div>
@endsection
