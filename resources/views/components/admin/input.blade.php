<div class="form-group ">
    <label class="form-label">{{$name}}</label>
    <input class="form-control @error($name)border border-danger @enderror" value="{{old($name,$value)}}" name="{{$name}}" @if($isView) readonly @endif/>
    @error($name)
        <p class="text-danger">{{$message}}</p>
    @enderror
</div>
